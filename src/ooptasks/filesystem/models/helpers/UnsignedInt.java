package ooptasks.filesystem.models.helpers;

public class UnsignedInt {
    private int data;

    public UnsignedInt(int data) throws IllegalArgumentException{
        setData(data);
    }

    private void setData(int data) throws IllegalArgumentException{
        if (data < 0) {
            throw new IllegalArgumentException();
        }
        else this.data = data;
    }

    public boolean isBigger(UnsignedInt ui) {
        return this.data > ui.data;
    }

    public int getData() { return data; }
}
