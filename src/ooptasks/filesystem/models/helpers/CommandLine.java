package ooptasks.filesystem.models.helpers;

import java.util.ArrayList;
import java.util.List;

public class CommandLine {

    private List<String> parameters;
    private String command;


    public CommandLine(String command) {
        this.parameters = new ArrayList<>();
        this.command = command;
    }

    public List<String> getParameters() {
        return new ArrayList<>(this.parameters);
    }

    public void addParameter(String parameter) {
        this.parameters.add(parameter);
    }
    public void setParameter(int index, String parameter) {
        this.parameters.set(index, parameter);
    }

    public String getCommand() {
        return this.command;
    }

}
