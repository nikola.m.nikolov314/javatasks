package ooptasks.filesystem.models.filesystems;

import ooptasks.filesystem.models.filesystems.FileSystem;

import java.io.File;

public class RealFileSystem extends FileSystem {
    private File currentDir;
    private File homeDir;

    public RealFileSystem() {
        super();
        homeDir = new File("D:\\");
        currentDir = homeDir;
    }

    public File getHomeDir() { return this.homeDir; }
    public File getCurrentDir() { return this.currentDir; }
    public void setCurrentDir(File f) { this.currentDir = f;}
}
