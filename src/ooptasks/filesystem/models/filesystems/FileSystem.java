package ooptasks.filesystem.models.filesystems;

import ooptasks.filesystem.commands.*;

import java.util.HashMap;

public class FileSystem {
     public static HashMap<String, Command> commands;

    public FileSystem() {
        commands = new HashMap<>();
        commands.put("mkdir", new MakeDir());
        commands.put("cd", new Cd());
        commands.put("create_file", new CreateFile());
        commands.put("cat", new Cat());
        commands.put("write", new Write());
        commands.put("ls", new Ls());
        commands.put("wc", new Wc());
        commands.put("|", new Pipe());
    }



}
