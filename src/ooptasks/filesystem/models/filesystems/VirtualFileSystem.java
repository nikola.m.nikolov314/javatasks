package ooptasks.filesystem.models.filesystems;

import ooptasks.filesystem.models.files.Folder;

public class VirtualFileSystem extends FileSystem {
    private Folder currentDir;
    private Folder homeDir;
    public VirtualFileSystem() {
        super();
        homeDir = new Folder("/home");
        currentDir = homeDir;
    }

    public Folder getHomeDir() { return this.homeDir; }
    public Folder getCurrentDir() { return this.currentDir; }
    public void setCurrentDir(Folder f) { this.currentDir = f;}
}
