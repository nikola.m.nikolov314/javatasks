package ooptasks.filesystem.models.files;

import ooptasks.filesystem.models.helpers.UnsignedInt;

import java.util.HashMap;
import java.util.Map;

public class TextFile extends File {
    private Map<UnsignedInt, String> content;

    public TextFile(String name, Folder parent) {
        this.name = name;
        content = new HashMap<>();
    }

    public Map<UnsignedInt, String> getContent() { return new HashMap<>(this.content); }

    public void append(String content, UnsignedInt line) {
        if (this.content.get(line) != null) {
            this.content.put(line, this.content.get(line) + content);
        }
        else this.content.put(line, content);
    }

    public void overwrite(String content, UnsignedInt line) {
        for (UnsignedInt existingLine : this.content.keySet()) {
            if (existingLine.getData() == line.getData()) {
                this.content.put(existingLine, content);
            }
        }
        this.content.put(line, content);
    }

    public int calculateSize() {
        int size = 0;
        for (String s : content.values()) {
            size += s.length();
        }
        size += this.getContent().size();
        return size;
    }
}
