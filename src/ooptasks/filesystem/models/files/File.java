package ooptasks.filesystem.models.files;

import java.util.ArrayList;


public abstract class File {
     String name;
     File parent;

    public File() {
        this.name = "";
    }

    public File getParent() {
        return this.parent;
    }
    public void setParent(File p) { this.parent = p; }

    public String getName() { return name; }

    public abstract int calculateSize();

    public String getPath() {
        if (this.getName().equals("/home")) {
            return "/";
        }
        ArrayList<String> names = new ArrayList<>();
        File f = this;
        while (!f.getName().equals("/home")) {
            names.add(f.getParent().getName());
            f = f.getParent();
        }
        StringBuilder path = new StringBuilder();
        for (int i = names.size() - 1; i >= 0; i--) {
            path.append(names.get(i)).append("/");
        }
        return path.toString();
    }
}
