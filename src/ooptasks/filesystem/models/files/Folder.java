package ooptasks.filesystem.models.files;

import java.util.ArrayList;
import java.util.List;

public class Folder extends File {
    private List<File> files;

    public Folder(String name, File parent) {
        this.name = name;
        this.parent = parent;
        files = new ArrayList<>();
    }

    public Folder(String name) {
        this.name = name;
        files = new ArrayList<>();
    }

    public List<File> getFiles() {
        return new ArrayList<>(this.files);
    }

    public void setFiles(List<File> files) { this.files = files; }

    public boolean addFile(File f) {
        for (File f1 : files) {
            if (f1.getName().equals(f.getName())) {
                System.out.println("There is already a file with the same name into the current directory");
                return false;
            }
        }
        files.add(f);
        return true;
    }

    public int calculateSize() {
        int size = 0;
        for (File f : this.files) {
            size += f.calculateSize();
        }
        return size;
    }

}
