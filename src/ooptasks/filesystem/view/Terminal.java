package ooptasks.filesystem.view;

import ooptasks.filesystem.models.filesystems.FileSystem;
import ooptasks.filesystem.models.filesystems.RealFileSystem;
import ooptasks.filesystem.models.filesystems.VirtualFileSystem;
import ooptasks.filesystem.models.helpers.CommandLine;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Terminal {
    private HashMap<Integer, FileSystem> fileSystems;

    public Terminal() {
        fileSystems = new HashMap<>();
        fileSystems.put(1, new VirtualFileSystem());
        fileSystems.put(2, new RealFileSystem());
    }

    public void run() {
        FileSystem fileSystem = fileSystems.get(chooseFileSystem());
        while (true) {
            List commandLines = readCommand();
            String oneLineCommand = transformCommandLines(commandLines);
            CommandLine commandLine = parser(oneLineCommand);
            if (!FileSystem.commands.containsKey(commandLine.getCommand())) {
                System.out.println("Invalid command");
            } else {
                try {

                    List<String> output = FileSystem.commands.get(commandLine.getCommand()).execute(fileSystem, commandLine);
                    printOutput(output);
                } catch (IllegalArgumentException e) {
                    System.out.println("Invalid arguments for: " + commandLine.getCommand());
                }
                catch (IOException e) {
                    System.out.println("File not found!");
                }
            }
        }

    }

    private int chooseFileSystem() {
        Scanner reader = new Scanner(System.in);

        System.out.println("If you want to work with virtual files press 1");
        System.out.println("If you want to work with real files press 2");
        int choice =  reader.nextInt();
        return choice;
    }

    private List readCommand() {
        List<String> commandLines = new ArrayList<>();
        Scanner reader = new Scanner(System.in);
        String command = reader.nextLine();
        commandLines.add(command);
        while(command.charAt(command.length() - 1) == '>') {
            command = reader.nextLine();
            commandLines.add(command);
        }
        return commandLines;
    }

    private String transformCommandLines(List<String> commandLines) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String s : commandLines) {
            stringBuilder.append(s);
        }
        return stringBuilder.toString();
    }

    private CommandLine parser(String command) {
        if (command.contains("|")) {
            CommandLine commandLine = new CommandLine("|");
            String[] parameters = command.split(" ");

            for (String parameter : parameters) {
                commandLine.addParameter(parameter);
            }
            return commandLine;
        }

        String[] commandWords = command.split(" ");
        CommandLine commandLine = new CommandLine(commandWords[0]);

        for (int i = 1; i < commandWords.length; i++) {
            commandLine.addParameter(commandWords[i]);
        }
        return commandLine;
    }

    private void printOutput(List<String> output) {
        for (String s : output) {
            System.out.println(s);
        }
    }
}

