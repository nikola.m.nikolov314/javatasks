package ooptasks.filesystem.commands;

import ooptasks.filesystem.models.filesystems.FileSystem;
import ooptasks.filesystem.models.filesystems.VirtualFileSystem;
import ooptasks.filesystem.models.helpers.CommandLine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Pipe implements Command {
    @Override
    public List<String> execute(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
        List<CommandLine> pipedCommands = generateCommands(commandLine.getParameters());
        for (CommandLine pipedCommand : pipedCommands) {
            validateInput(pipedCommand);
        }
        return executePipedCommands(pipedCommands, fileSystem);
    }

    private List<CommandLine> generateCommands(List<String> arguments) {
        List<CommandLine> pipedCommands = new ArrayList<>();
        for (int i = 0; i < arguments.size(); i++) {
            CommandLine commandLine = new CommandLine(arguments.get(i++));
            while (i < arguments.size()
                    && !arguments.get(i).equals("|")) {
                commandLine.addParameter(arguments.get(i++));
            }
            pipedCommands.add(commandLine);
        }
        return pipedCommands;
    }
    @Override
    public void validateInput(CommandLine commandLine) {
        if (!VirtualFileSystem.commands.containsKey(commandLine.getCommand())) {
            throw new IllegalArgumentException();
        }
    }

    private List<String> executePipedCommands(List<CommandLine> pipedCommands, FileSystem fileSystem)
            throws IOException {
        List<String> lastOutput = new ArrayList<>();
        for (CommandLine currentCommand : pipedCommands) {
            if (lastOutput.size() > 0) {
                for (String s : lastOutput) {
                    currentCommand.addParameter(s);
                }
            }
            lastOutput = FileSystem.commands.get(currentCommand.getCommand()).execute(fileSystem, currentCommand);
        }
        return lastOutput;
    }
}
