package ooptasks.filesystem.commands;

import ooptasks.filesystem.models.files.File;
import ooptasks.filesystem.models.files.Folder;
import ooptasks.filesystem.models.files.TextFile;
import ooptasks.filesystem.models.filesystems.FileSystem;
import ooptasks.filesystem.models.filesystems.RealFileSystem;
import ooptasks.filesystem.models.filesystems.VirtualFileSystem;
import ooptasks.filesystem.models.helpers.CommandLine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Wc implements Command {
    @Override
    public List<String> execute(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
        validateInput(commandLine);
        if (hasLinesOption(commandLine)) {
            if (commandLine.getParameters().size() == 1) {
                return CommandsServices.loadOutput("The number of lines in the text is: 1");
            }
            if (isRemoteDir(commandLine, 1)) {
                List<String> output = countLinesInFileInRemoteDir(fileSystem, commandLine);
                if (output != null) {
                    return output;
                }
            }
            List<String> output = countLinesInFileInCurrentDir(fileSystem, commandLine);
            if (output != null) {
                return output;
            }
            int numberOfLines = commandLine.getParameters().get(1).split(">").length;
            return CommandsServices.loadOutput("The number of lines in the text is: " + numberOfLines);
        }

        if (isRemoteDir(commandLine, 0)) {
            List<String> output = countWordsInFileInRemoteDir(fileSystem, commandLine);
            if (output != null) {
                return output;
            }
        }
        List<String> output = countWordsInFileInCurrentDir(fileSystem, commandLine);
        if (output != null) {
            return output;
        }
        return CommandsServices.loadOutput("The number of words in the text is: "
                + (commandLine.getParameters().size()
                + countNewLineSymbols(commandLine.getParameters())));
    }

    private int countNewLineSymbols(List<String> stringList) {
        int counter = 0;
        for (String s : stringList) {
            for (char c : s.toCharArray()) {
                if (c == '>') {
                    counter++;
                }
            }
        }
        return counter;
    }

    private int wordsInFile(File f) {
        int numberOfWords = 0;
        for (String s : ((TextFile) f).getContent().values()) {
            numberOfWords += s.split(" ").length;
        }
        return numberOfWords;
    }

    @Override
    public void validateInput(CommandLine commandLine) {
        if (commandLine.getParameters().size() == 0) {
            throw  new IllegalArgumentException();
        }
    }

    private List<String> countLinesInFileInRemoteDir(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {

        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return countLinesInVirtualFileInRemoteDir((VirtualFileSystem) fileSystem, commandLine);
        }
        return countLinesInRealFileInRemoteDir((RealFileSystem) fileSystem, commandLine);
    }



    private List<String> countLinesInFileInCurrentDir(FileSystem fileSystem, CommandLine commandLine) throws IOException {
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return countLinesInVirtualFileInCurrentDir((VirtualFileSystem) fileSystem, commandLine);
        }

        return countLinesInRealFileInCurrentDir((RealFileSystem) fileSystem, commandLine);
    }

    private int countLinesInRealFile(java.io.File file) throws IOException {
        List<String> output = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        while ((line = br.readLine()) != null) {
            output.add(line);
        }
        return output.size();
    }

    private int countWordsInRealFile(java.io.File file) throws IOException {
        List<String> output = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        while ((line = br.readLine()) != null) {
            output.add(line);
        }
        int numberOfWords = 0;
        for (String s : output) {
            numberOfWords += s.split(" ").length;
        }
        return numberOfWords;
    }

    private List<String> countWordsInFileInRemoteDir(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return countWordsInVirtualFileInRemoteDir((VirtualFileSystem) fileSystem, commandLine);
        }
        return countWordsInRealFileInRemoteDir((RealFileSystem) fileSystem, commandLine);
    }

    private List<String> countWordsInFileInCurrentDir(FileSystem fileSystem, CommandLine commandLine) throws IOException {
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return countWordsInVirtualFileInCurrentDir((VirtualFileSystem)fileSystem, commandLine);
        }
        return countWordsInRealFileInCurrentDir((RealFileSystem) fileSystem, commandLine);
    }

    private boolean hasLinesOption(CommandLine commandLine) {
       return commandLine.getParameters().get(0).equals("-l");
    }

    private boolean isRemoteDir(CommandLine commandLine, int pathIndex) {
        return commandLine.getParameters().get(pathIndex).contains("/")
                || commandLine.getParameters().get(pathIndex).contains("\\");
    }

    private List<String> countLinesInVirtualFileInRemoteDir(VirtualFileSystem fileSystem, CommandLine commandLine) throws IOException {
        String fileName = CommandsServices.generateFileName(commandLine, 1);
        Folder currentDir = fileSystem.getCurrentDir();
        CommandsServices.executeCd(fileSystem, commandLine, 1);

        for (File f : fileSystem.getCurrentDir().getFiles()) {
            if (f.getName().equals(fileName)
                    && f.getClass() == TextFile.class
                    && commandLine.getParameters().size() == 2) {
                List<String> output = CommandsServices.loadOutput("The number of lines in the file is: "
                        + ((TextFile) f).getContent().values().size());
                fileSystem.setCurrentDir(currentDir);
                return output;
            }
        }
        fileSystem.setCurrentDir(currentDir);
        return null;
    }

    private List<String> countLinesInRealFileInRemoteDir(RealFileSystem fileSystem, CommandLine commandLine) throws IOException {
        Path filePath = generateFilePath(fileSystem, commandLine, 1);
        String fileName = generateFileName(commandLine, 1);
        return findFileAndCountLines(filePath , fileName);
    }

    private Path generateFilePath(RealFileSystem fileSystem, CommandLine commandLine, int pathIndex) {
        String stringPath = commandLine.getParameters().get(pathIndex).substring(0, commandLine.getParameters().get(pathIndex).lastIndexOf('\\'));
        if (!stringPath.contains("D:\\")) {
            stringPath = fileSystem.getCurrentDir().getPath() + "\\" + stringPath;
        }
        Path path = Paths.get(stringPath);
        return path;
    }

    private String generateFileName(CommandLine commandLine, int nameIndex) {
        return commandLine.getParameters().get(nameIndex).substring(commandLine.getParameters().get(nameIndex).lastIndexOf('\\') + 1);
    }

    private List<String> findFileAndCountLines(String fileName, RealFileSystem fileSystem) throws IOException {
        java.io.File[] files = fileSystem.getCurrentDir().listFiles();
        if (files != null) {
            for (java.io.File f : files) {
                if (f.getName().equals(fileName)) {
                    List<String> output = CommandsServices.loadOutput("The number of lines in the file is: "
                            + countLinesInRealFile(f) );
                    return output;
                }
            }
        }
        return null;
    }

    private List<String> findFileAndCountLines(Path filePath, String fileName) throws IOException {
        java.io.File[] files = filePath.toFile().listFiles();
        if (files != null) {
            for (java.io.File f : files) {
                if (f.getName().equals(fileName)) {
                    List<String> output = CommandsServices.loadOutput("The number of lines in the file is: "
                            + countLinesInRealFile(f) );
                    return output;
                }
            }
        }
        return null;
    }

    private List<String> countLinesInVirtualFileInCurrentDir(VirtualFileSystem fileSystem, CommandLine commandLine) {
        for (File f : fileSystem.getCurrentDir().getFiles()) {
            if (f.getName().equals(commandLine.getParameters().get(1))
                    && f.getClass() == TextFile.class
                    && commandLine.getParameters().size() == 2) {
                return CommandsServices.loadOutput("The number of lines in the file is: "
                        + ((TextFile) f).getContent().values().size());
            }
        }
        return null;
    }

    private List<String> countLinesInRealFileInCurrentDir(RealFileSystem fileSystem, CommandLine commandLine) throws IOException {
        String fileName = commandLine.getParameters().get(1);
        return findFileAndCountLines(fileName, fileSystem);
    }

    private List<String> findFileAndCountWords(Path filePath, String fileName) throws IOException {
        java.io.File[] files = filePath.toFile().listFiles();
        if (files != null) {
            for (java.io.File f : files) {
                if (f.getName().equals(fileName)) {
                    List<String> output = CommandsServices.loadOutput("The number of words in the file is: "
                            + countWordsInRealFile(f) );
                    return output;
                }
            }
        }
        return null;
    }

    private List<String> countWordsInRealFileInRemoteDir(RealFileSystem fileSystem, CommandLine commandLine) throws IOException {
        Path filePath = generateFilePath(fileSystem, commandLine, 0);
        String fileName = generateFileName(commandLine, 0);
        return findFileAndCountWords(filePath, fileName);

    }

    private List<String> countWordsInVirtualFileInRemoteDir(VirtualFileSystem fileSystem, CommandLine commandLine) throws IOException {
        String fileName = CommandsServices.generateFileName(commandLine, 0);
        Folder currentDir = fileSystem.getCurrentDir();
        CommandsServices.executeCd(fileSystem, commandLine, 0);
        for (File f : fileSystem.getCurrentDir().getFiles()) {
            if (f.getName().equals(fileName)
                    && f.getClass() == TextFile.class
                    && commandLine.getParameters().size() == 1) {
                fileSystem.setCurrentDir(currentDir);
                return CommandsServices.loadOutput("The number of words in the file is: "
                        + wordsInFile(f));
            }
        }
        fileSystem.setCurrentDir(currentDir);
        return null;
    }

    private List<String> countWordsInVirtualFileInCurrentDir(VirtualFileSystem fileSystem, CommandLine commandLine) {
        for (File f : fileSystem.getCurrentDir().getFiles()) {
            if (f.getName().equals(commandLine.getParameters().get(0))
                    && f.getClass() == TextFile.class
                    && commandLine.getParameters().size() == 1) {
                return CommandsServices.loadOutput("The number of words in the file is: " + wordsInFile(f));
            }
        }
        return null;
    }

    private List<String> countWordsInRealFileInCurrentDir(RealFileSystem fileSystem, CommandLine commandLine) throws IOException {
        String fileName = commandLine.getParameters().get(0);
        java.io.File[] files = fileSystem.getCurrentDir().listFiles();
        if (files != null) {
            for (java.io.File f : files) {
                if (f.getName().equals(fileName)) {
                    List<String> output = CommandsServices.loadOutput("The number of words in the file is: "
                            + countWordsInRealFile(f) );
                    return output;
                }
            }
        }
        return null;
    }
}
