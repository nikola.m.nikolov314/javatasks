package ooptasks.filesystem.commands;

import ooptasks.filesystem.models.files.Folder;
import ooptasks.filesystem.models.files.TextFile;
import ooptasks.filesystem.models.filesystems.FileSystem;
import ooptasks.filesystem.models.filesystems.RealFileSystem;
import ooptasks.filesystem.models.filesystems.VirtualFileSystem;
import ooptasks.filesystem.models.helpers.CommandLine;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class CreateFile implements Command{
    public List<String> execute(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
        validateInput(commandLine);
        if (commandLine.getParameters().get(0).contains("/") || commandLine.getParameters().get(0).contains("\\")) {
            return createFileInRemoteDir(fileSystem, commandLine);
        }
        return createFileInCurrentDir(fileSystem, commandLine);
    }

    @Override
    public void validateInput(CommandLine commandLine) {
        if (commandLine.getParameters().size() > 1
                || commandLine.getParameters().size() == 0
                || commandLine.getParameters().get(0).split(" ").length > 1) {
            throw new IllegalArgumentException();
        }

        for (char c : CommandsServices.INVALID_CHARS) {
            if (commandLine.getParameters().get(0).contains(Character.toString(c))) {
                throw new IllegalArgumentException();
            }
        }
    }

    private List<String> createFileInRemoteDir(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return createVirtualFileInRemoteDir((VirtualFileSystem) fileSystem, commandLine);
        }
        return createRealFileInRemoteDir((RealFileSystem) fileSystem, commandLine);
    }

    private List<String> createFileInCurrentDir(FileSystem fileSystem, CommandLine commandLine) throws IOException {
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            ((VirtualFileSystem) fileSystem).getCurrentDir().addFile(new TextFile(commandLine.getParameters().get(0),
                    ((VirtualFileSystem) fileSystem).getCurrentDir()));

            return CommandsServices.loadOutput("Successfully created a file");
        }
        new File(((RealFileSystem) fileSystem).getCurrentDir().getPath() + commandLine.getParameters().get(0)).createNewFile();
        return CommandsServices.loadOutput("Successfully created a file");
    }

    private List<String> createVirtualFileInRemoteDir(VirtualFileSystem fileSystem, CommandLine commandLine) throws IOException {
        String fileName = CommandsServices.generateFileName(commandLine, 0);
        Folder currentDir = fileSystem.getCurrentDir();
        CommandsServices.executeCd(fileSystem, commandLine, 0);
        fileSystem.getCurrentDir().addFile(new TextFile(fileName, fileSystem.getCurrentDir()));
        fileSystem.setCurrentDir(currentDir);
        return CommandsServices.loadOutput("Successfully created a file");
    }

    private List<String> createRealFileInRemoteDir(RealFileSystem fileSystem, CommandLine commandLine) throws IOException {
        String filePath;
        if (!commandLine.getParameters().get(0).contains("D:\\")) {
            filePath = fileSystem.getCurrentDir().getAbsolutePath() + commandLine.getParameters().get(0);
        }
        else filePath = commandLine.getParameters().get(0);
        new File(filePath).createNewFile();
        return CommandsServices.loadOutput("Successfully created a file");
    }
}
