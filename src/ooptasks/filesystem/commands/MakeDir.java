package ooptasks.filesystem.commands;
import ooptasks.filesystem.models.filesystems.FileSystem;
import ooptasks.filesystem.models.filesystems.RealFileSystem;
import ooptasks.filesystem.models.filesystems.VirtualFileSystem;
import ooptasks.filesystem.models.helpers.CommandLine;
import ooptasks.filesystem.models.files.Folder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class MakeDir implements Command {

    @Override
    public List<String> execute(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
        validateInput(commandLine);
        if (commandLine.getParameters().get(0).contains("/") || commandLine.getParameters().get(0).contains("\\")) {
            return makeDirInRemoteDir(fileSystem, commandLine);
        }
        return makeDirInCurrentDir(fileSystem, commandLine);
    }

    @Override
    public void validateInput(CommandLine commandLine) {
        if (commandLine.getParameters().size() > 1
                || commandLine.getParameters().get(0).split(" ").length > 1) {
            throw new IllegalArgumentException();
        }
        for (char c : CommandsServices.INVALID_CHARS) {
            if (commandLine.getParameters().get(0).contains(Character.toString(c))) {
                throw new IllegalArgumentException();
            }
        }
    }

    private String generateDirName(CommandLine commandLine) {
            return commandLine
                    .getParameters()
                    .get(0)
                    .split("/")[commandLine
                    .getParameters()
                    .get(0).split("/")
                    .length - 1];
    }

    private List<String> makeDirInRemoteDir(FileSystem fileSystem, CommandLine commandLine) throws IOException {
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return makeVirtualDirInRemoteDir((VirtualFileSystem) fileSystem, commandLine);
        }
        return makeRealDirInRemoteDir((RealFileSystem) fileSystem, commandLine);
    }

    private List<String> makeDirInCurrentDir(FileSystem fileSystem, CommandLine commandLine) {
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return makeVirtualDirInCurrentDir((VirtualFileSystem) fileSystem, commandLine);
        }
        return makeRealDirInCurrentDir((RealFileSystem) fileSystem, commandLine);
    }

    private List<String> makeVirtualDirInRemoteDir(VirtualFileSystem fileSystem, CommandLine commandLine) throws IOException {
        Folder currentDir = fileSystem.getCurrentDir();
        String dirName = generateDirName(commandLine);
        CommandsServices.executeCd(fileSystem, commandLine, 0);
        fileSystem.getCurrentDir().addFile(new Folder(dirName, fileSystem.getCurrentDir()));
        fileSystem.setCurrentDir(currentDir);
        return CommandsServices.loadOutput("Successfully created a directory");
    }

    private List<String> makeRealDirInRemoteDir(RealFileSystem fileSystem, CommandLine commandLine) throws FileNotFoundException {
        String filePath = CommandsServices.generateRealFilePath(fileSystem, commandLine, 0);
        if (new File(filePath).mkdir()) {
            return CommandsServices.loadOutput("Successfully created a directory");
        }
        throw new FileNotFoundException();
    }

    private List<String> makeVirtualDirInCurrentDir(VirtualFileSystem fileSystem, CommandLine commandLine) {
        fileSystem.getCurrentDir().addFile(new Folder(commandLine.getParameters().get(0), fileSystem.getCurrentDir()));
        return CommandsServices.loadOutput("Successfully created a directory");
    }

    private List<String> makeRealDirInCurrentDir(RealFileSystem fileSystem, CommandLine commandLine) {
        new File(fileSystem.getCurrentDir().getPath() + "\\" + commandLine.getParameters().get(0)).mkdir();
        return CommandsServices.loadOutput("Successfully created a directory");
    }
}
