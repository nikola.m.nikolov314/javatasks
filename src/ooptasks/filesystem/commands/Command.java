package ooptasks.filesystem.commands;

import ooptasks.filesystem.models.filesystems.FileSystem;
import ooptasks.filesystem.models.helpers.CommandLine;


import java.io.IOException;
import java.util.List;

public interface Command {
     List<String> execute(FileSystem fileSystem, CommandLine commandLine)
             throws IllegalArgumentException, IOException;

     void validateInput(CommandLine commandLine) throws IllegalArgumentException;
}
