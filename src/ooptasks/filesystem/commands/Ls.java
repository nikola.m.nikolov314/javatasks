package ooptasks.filesystem.commands;

import ooptasks.filesystem.models.files.File;
import ooptasks.filesystem.models.files.Folder;
import ooptasks.filesystem.models.filesystems.FileSystem;
import ooptasks.filesystem.models.filesystems.RealFileSystem;
import ooptasks.filesystem.models.filesystems.VirtualFileSystem;
import ooptasks.filesystem.models.helpers.CommandLine;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Ls implements Command {
    @Override
    public List<String> execute(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
        validateInput(commandLine);
        if (commandLine.getParameters().size() > 0) {
            if (hasSortedOptionInRemoteDir(commandLine)) {
                return lsSortedRemoteDir(fileSystem, commandLine);
            }
            else if (hasSortedOptionInCurrentDir(commandLine)) {
                return lsSortedCurrentDir(fileSystem);
            }
            else if (isRemoteDir(commandLine)) {
                return lsRemoteDir(fileSystem, commandLine);
            }
            else if (isFirstLevelSubDir(commandLine)) {
                return lsFirstLevelSubDir(fileSystem, commandLine);
            }
        }
        else {
            return lsCurrentDir(fileSystem);
        }
        throw new IllegalArgumentException();
    }

    @Override
    public void validateInput(CommandLine commandLine) {
        for (char c : CommandsServices.INVALID_CHARS) {
            if (commandLine.getParameters().size() > 0
                    && commandLine.getParameters().get(0).contains(Character.toString(c))) {
                throw new IllegalArgumentException();

            }
        }
    }

    private List<File> sortFiles(List<File> files) {
        for (int i = 0; i < files.size() - 1; i++) {
            for (int j = i + 1; j < files.size(); j++) {
                if (files.get(i).calculateSize() < files.get(j).calculateSize()) {
                    File f = files.get(i);
                    files.set(i, files.get(j));
                    files.set(j, f);
                }
            }
        }
        return files;
    }

    private void executeCd(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
        CommandLine cdCommandLine = CommandsServices.generateCdCommandLine(commandLine.getParameters().get(0));
        FileSystem.commands.get(cdCommandLine.getCommand()).execute(fileSystem, cdCommandLine);
        cdCommandLine.setParameter(0, commandLine.getParameters().get(0)
                .split("/")[commandLine.getParameters().get(0).split("/").length - 1]);

        FileSystem.commands.get(cdCommandLine.getCommand()).execute(fileSystem, cdCommandLine);
    }

    private List<String> lsCurrentDir(FileSystem fileSystem) {
            if (fileSystem.getClass() == VirtualFileSystem.class) {
                return lsCurrentVirtualDir((VirtualFileSystem) fileSystem);
            }
            return lsCurrentRealDir((RealFileSystem) fileSystem);
    }

    private List<String> lsSortedRemoteDir(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return lsSortedRemoteVirtualDir((VirtualFileSystem) fileSystem, commandLine);
        }
        return lsSortedRemoteRealDir((RealFileSystem) fileSystem, commandLine);
    }

    private List<String> lsSortedCurrentDir(FileSystem fileSystem) {
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return lsSortedCurrentVirtualDir((VirtualFileSystem) fileSystem);
        }
        return lsSortedCurrentRealDir((RealFileSystem) fileSystem);
    }

    private List<String> lsRemoteDir(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return lsRemoteVirtualDir((VirtualFileSystem) fileSystem, commandLine);
        }
        return lsRemoteRealDir((RealFileSystem) fileSystem, commandLine);
    }

    private List<String> lsFirstLevelSubDir(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
            if (fileSystem.getClass() == VirtualFileSystem.class) {
                return lsFirstLevelSubVirtualDir((VirtualFileSystem) fileSystem, commandLine);
            }
        return lsRemoteRealDir((RealFileSystem) fileSystem, commandLine);
    }

    private boolean hasSortedOptionInRemoteDir(CommandLine commandLine) {
        return commandLine.getParameters().get(0).equals("--sorted")
                && commandLine.getParameters().get(1).equals("desc")
                && commandLine.getParameters().size() == 3;
    }

    private boolean hasSortedOptionInCurrentDir(CommandLine commandLine) {
        return commandLine.getParameters().get(0).equals("--sorted")
                && commandLine.getParameters().get(1).equals("desc")
                && commandLine.getParameters().size() == 2;
    }

    private boolean isRemoteDir(CommandLine commandLine) {
        return commandLine.getParameters().size() == 1
                && (commandLine.getParameters().get(0).contains("/") || commandLine.getParameters().get(0).contains("\\"));
    }

    private boolean isFirstLevelSubDir(CommandLine commandLine) {
        return commandLine.getParameters().get(0).split(" ").length == 1
                && !commandLine.getParameters().get(0).contains("/") && !commandLine.getParameters().get(0).contains("\\");
    }

    private List<String> lsSortedRemoteVirtualDir(VirtualFileSystem fileSystem, CommandLine commandLine) throws IOException {
        Folder currentDir = fileSystem.getCurrentDir();
        CommandsServices.executeCd(fileSystem, commandLine, 2);
        fileSystem.getCurrentDir().setFiles(sortFiles(fileSystem.getCurrentDir().getFiles()));
        List<String> output = lsCurrentDir(fileSystem);
        fileSystem.setCurrentDir(currentDir);
        return output;
    }

    private List<String> lsSortedRemoteRealDir(RealFileSystem fileSystem, CommandLine commandLine) throws IOException {
        String stringPath = CommandsServices.generateRealFilePath(fileSystem, commandLine, 2);
        Path path = Paths.get(stringPath);
        List<String> output = new ArrayList<>();
        if (path.toFile().isDirectory()) {
            java.io.File[] files = path.toFile().listFiles();
            if (files != null) {
                return Arrays.stream(files).sorted(Comparator.comparing(CommandsServices::getRealFileSize).reversed())
                        .map(java.io.File::getName).collect(Collectors.toList());

            }
            output.add("");
            return output;
        }
        throw new FileNotFoundException();
    }

    private List<String> lsSortedCurrentVirtualDir(VirtualFileSystem fileSystem) {
        fileSystem.getCurrentDir().setFiles(sortFiles(fileSystem.getCurrentDir().getFiles()));
        return lsCurrentDir(fileSystem);
    }

    private List<String> lsSortedCurrentRealDir(RealFileSystem fileSystem) {
        java.io.File[] files =  fileSystem.getCurrentDir().listFiles();
        if (files != null) {
            return Arrays.stream(files).sorted(Comparator.comparing(CommandsServices::getRealFileSize).reversed())
                    .map(java.io.File::getName).collect(Collectors.toList());
        }
        List<String> output = new ArrayList<>();
        output.add("");
        return output;
    }

    private List<String> lsRemoteVirtualDir(VirtualFileSystem fileSystem, CommandLine commandLine) throws IOException {
        Folder currentDir = fileSystem.getCurrentDir();
        executeCd(fileSystem, commandLine);
        List<String> output = lsCurrentDir(fileSystem);
        fileSystem.setCurrentDir(currentDir);
        return output;
    }

    private List<String> lsRemoteRealDir(RealFileSystem fileSystem, CommandLine commandLine) throws FileNotFoundException {
        String stringPath = CommandsServices.generateRealFilePath(fileSystem, commandLine, 0);
        Path path = Paths.get(stringPath);
        List<String> output = new ArrayList<>();
        if (path.toFile().isDirectory()) {
            java.io.File[] files = path.toFile().listFiles();
            if (files != null) {
                return Arrays.stream(files).map(java.io.File::getName).collect(Collectors.toList());
            }
            output.add("");
            return output;
        }
        throw new FileNotFoundException();
    }

    private List<String> lsFirstLevelSubVirtualDir(VirtualFileSystem fileSystem, CommandLine commandLine) throws IOException {
        Folder currentDir = fileSystem.getCurrentDir();
        CommandsServices.executeCd(fileSystem, commandLine, 0);
        List<String> output = lsCurrentDir(fileSystem);
        fileSystem.setCurrentDir(currentDir);
        return output;
    }

    private List<String> lsCurrentRealDir(RealFileSystem fileSystem) {
        if (fileSystem.getCurrentDir().list() != null) {
            return Arrays.asList(fileSystem.getCurrentDir().list());
        }
        List<String> output = new ArrayList<>();
        output.add("");
        return output;
    }

    private List<String> lsCurrentVirtualDir(VirtualFileSystem fileSystem) {
        List<String> output = new ArrayList<>();
        if (fileSystem.getCurrentDir().getFiles().size() == 0) {
            output.add("");
            return output;
        }
        for (File f : fileSystem.getCurrentDir().getFiles()) {
            if (!f.getName().equals(fileSystem.getCurrentDir().getName())) {
                output.add(f.getName());
            }
        }
        return output;
    }
}
