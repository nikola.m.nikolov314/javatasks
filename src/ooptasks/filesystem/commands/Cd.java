package ooptasks.filesystem.commands;
import ooptasks.filesystem.models.files.File;
import ooptasks.filesystem.models.files.Folder;
import ooptasks.filesystem.models.filesystems.FileSystem;
import ooptasks.filesystem.models.filesystems.RealFileSystem;
import ooptasks.filesystem.models.filesystems.VirtualFileSystem;
import ooptasks.filesystem.models.helpers.CommandLine;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Cd implements Command {
    @Override
    public List<String> execute(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
        validateInput(commandLine);
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return cdInVirtualFileSystem((VirtualFileSystem) fileSystem, commandLine);
        }
        return cdInRealFileSystem((RealFileSystem) fileSystem, commandLine);
    }
    
    @Override
    public void validateInput(CommandLine commandLine) {
        if (commandLine.getParameters().size() > 1
                || commandLine.getParameters().get(0).split(" ").length > 1) {
            throw new IllegalArgumentException();
        }

        for (char c : CommandsServices.INVALID_CHARS) {
            if (commandLine.getParameters().get(0).contains(Character.toString(c))) {
                throw new IllegalArgumentException();
            }
        }
    }

    private File getFile(List<File> files, String name) {
        for (File f : files) {
            if (f.getName().equals(name)) {
                return f;
            }
        }
        return null;
    }

    private List<String> loadOutput(String path, String name) {
        List<String> output = new ArrayList<>();
        output.add("Current dir path: " + path);
        output.add("Current dir name: " + name);
        return output;
    }

    private List<String> loadOutput(String path) {
        List<String> output = new ArrayList<>();
        output.add("Current dir path: " + path);
        return output;
    }


    private List<String> cdDirWithIncompletePath(File f, CommandLine commandLine, FileSystem fileSystem)
            throws IOException {
        String remainingPath = f.getPath();
        String fullPath = remainingPath + commandLine.getParameters().get(0);
        CommandLine innerCd = new CommandLine("cd");
        innerCd.addParameter(fullPath);
        return FileSystem.commands.get(innerCd.getCommand()).execute(fileSystem, innerCd);
    }

    private List<String> cdDir(VirtualFileSystem fileSystem, File f, CommandLine commandLine, String[] pathWords)
            throws FileNotFoundException {
        int index = 2;
        while (!(f.getPath() + f.getName()).equals(commandLine.getParameters().get(0))) {
            f = getFile(((Folder) f).getFiles(), pathWords[index++]);

            if (f == null) {
                throw new FileNotFoundException();
            }
        }
        fileSystem.setCurrentDir((Folder) f);
        return loadOutput(fileSystem.getCurrentDir().getPath(), fileSystem.getCurrentDir().getName());
    }

    private List<String> cdDir(VirtualFileSystem fileSystem, CommandLine commandLine) throws FileNotFoundException {
        for (File f : fileSystem.getCurrentDir().getFiles()) {
            if (f.getName().equals(commandLine.getParameters().get(0))
                    && f.getClass() == Folder.class) {
                fileSystem.setCurrentDir((Folder) f);
                return loadOutput(fileSystem.getCurrentDir().getPath(), fileSystem.getCurrentDir().getName());
            }
        }
        throw new FileNotFoundException();
    }

    private List<String> cdInVirtualFileSystem(VirtualFileSystem fileSystem, CommandLine commandLine) throws IOException {
        if (hasToStayInCurrentDir(fileSystem, commandLine)) {
            return loadOutput(fileSystem.getCurrentDir().getPath(), fileSystem.getCurrentDir().getName());
        } else if (commandLine.getParameters().get(0).equals("..")) {
            fileSystem.setCurrentDir((Folder) fileSystem.getCurrentDir().getParent());
            return loadOutput(fileSystem.getCurrentDir().getPath(), fileSystem.getCurrentDir().getName());
        } else if (commandLine.getParameters().get(0).contains("/")) {

            String[] pathWords = commandLine.getParameters().get(0).split("/");

            if (pathWords.length > 1 && pathWords[1].equals("home")) {
                if (pathWords.length == 2) {
                    fileSystem.setCurrentDir(fileSystem.getHomeDir());
                    return loadOutput(fileSystem.getCurrentDir().getPath(), fileSystem.getCurrentDir().getName());
                }
                File homeDir = fileSystem.getHomeDir();
                return cdDir(fileSystem, homeDir, commandLine, pathWords);
            } else {
                File f = getFile(fileSystem.getCurrentDir().getFiles(), pathWords[0]);
                if (f == null) {
                    throw new FileNotFoundException();
                }
                return cdDirWithIncompletePath(f, commandLine, fileSystem);
            }
        }
        return cdDir(fileSystem, commandLine);
    }

    private List<String> cdInRealFileSystem(RealFileSystem fileSystem, CommandLine commandLine) {
        if (hasToStayInCurrentDir(fileSystem, commandLine)) {
            return loadOutput(fileSystem.getCurrentDir().getPath());
        } else if (commandLine.getParameters().get(0).equals("..")) {
            fileSystem.setCurrentDir(fileSystem.getCurrentDir().getParentFile());
            return loadOutput(fileSystem.getCurrentDir().getPath());
        }

        if (commandLine.getParameters().get(0).contains("D:\\")) {
            Path path = Paths.get(commandLine.getParameters().get(0));
            fileSystem.setCurrentDir(path.toFile());
            return loadOutput(fileSystem.getCurrentDir().getPath());
        }

        Path path = Paths.get(fileSystem.getCurrentDir().getPath() + "\\" + commandLine.getParameters().get(0));
        fileSystem.setCurrentDir(path.toFile());
        return loadOutput(fileSystem.getCurrentDir().getPath());
    }

    private boolean hasToStayInCurrentDir(RealFileSystem fileSystem, CommandLine commandLine) {
        return commandLine.getParameters().get(0).equals(".")
                || (commandLine.getParameters().get(0).equals("..")
                && fileSystem.getCurrentDir().getPath().equals("D:\\"));
    }

    private boolean hasToStayInCurrentDir(VirtualFileSystem fileSystem, CommandLine commandLine) {
        return commandLine.getParameters().get(0).equals(".")
                || (commandLine.getParameters().get(0).equals("..")
                && fileSystem.getCurrentDir().getName().equals("/home"));
    }
}
