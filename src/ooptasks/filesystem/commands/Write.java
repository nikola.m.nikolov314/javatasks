package ooptasks.filesystem.commands;

import ooptasks.filesystem.models.files.File;
import ooptasks.filesystem.models.files.Folder;
import ooptasks.filesystem.models.files.TextFile;
import ooptasks.filesystem.models.filesystems.FileSystem;
import ooptasks.filesystem.models.filesystems.RealFileSystem;
import ooptasks.filesystem.models.filesystems.VirtualFileSystem;
import ooptasks.filesystem.models.helpers.UnsignedInt;
import ooptasks.filesystem.models.helpers.CommandLine;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Write implements Command {
    @Override
    public List<String> execute(FileSystem fileSystem, CommandLine commandLine) throws IOException {
        validateInput(commandLine);
        if (commandLine.getParameters().get(0).equals("-overwrite")) {
            if (commandLine.getParameters().get(1).contains("/") || commandLine.getParameters().get(1).contains("\\")) {
                return overwriteIntoFileInRemoteDir(fileSystem, commandLine);
            }
            return overwriteIntoFileInCurrentDir(fileSystem, commandLine);
        }
        if (commandLine.getParameters().get(0).contains("/") || commandLine.getParameters().get(0).contains("\\")) {
            return writeIntoFileInRemoteDir(fileSystem, commandLine);
        }
        return writeIntoFileInCurrentDir(fileSystem, commandLine);
    }

    @Override
    public void validateInput(CommandLine commandLine) {
        if (commandLine.getParameters().size() < 1) {
            throw new IllegalArgumentException();
        }
    }

    private String generateInput(CommandLine commandLine, int beginIndex) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = beginIndex; i < commandLine.getParameters().size(); i++) {
            stringBuilder.append(commandLine.getParameters().get(i)).append(" ");
        }
        return stringBuilder.toString();
    }

    private List<String> writeIntoFileInRemoteDir(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return writeIntoVirtualFileInRemoteDir((VirtualFileSystem) fileSystem, commandLine);
        }

        return writeIntoRealFile((RealFileSystem) fileSystem, commandLine);
    }

    private List<String> writeIntoFileInCurrentDir(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return writeIntoVirtualFileInCurrentDir((VirtualFileSystem) fileSystem, commandLine);
        }
        return writeIntoRealFile((RealFileSystem) fileSystem, commandLine);
    }

    private List<String> overwriteIntoFileInRemoteDir(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return overwriteIntoVirtualFileInRemoteDir((VirtualFileSystem) fileSystem, commandLine);
        }
        return overwriteIntoRealFile((RealFileSystem) fileSystem, commandLine);
    }

    private List<String> overwriteIntoFileInCurrentDir(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return overwriteIntoVirtualFileInCurrentDir((VirtualFileSystem) fileSystem, commandLine);
        }
        return overwriteIntoRealFile((RealFileSystem) fileSystem, commandLine);
    }

    private List<String> overwriteIntoVirtualFileInRemoteDir(VirtualFileSystem fileSystem, CommandLine commandLine) throws IOException {
        String fileName = CommandsServices.generateFileName(commandLine, 1);
        Folder currentDir = fileSystem.getCurrentDir();
        CommandsServices.executeCd(fileSystem, commandLine, 1);

        for (File f : fileSystem.getCurrentDir().getFiles()) {
            if (f.getName().equals(fileName)) {
                if (f.getClass() != TextFile.class) {
                    fileSystem.setCurrentDir(currentDir);
                    throw new IllegalArgumentException();
                }
                String input = generateInput(commandLine, 3);
                try {
                    UnsignedInt line = new UnsignedInt(Integer.parseInt(commandLine.getParameters().get(2)));
                    ((TextFile) f).overwrite(input, line);
                    fileSystem.setCurrentDir(currentDir);
                    return CommandsServices.loadOutput("Successfully wrote into the file");
                }
                catch (IllegalArgumentException e) {
                    fileSystem.setCurrentDir(currentDir);
                    throw new IllegalArgumentException();
                }
            }
        }
        throw new FileNotFoundException();
    }

    public List<String> overwriteIntoRealFile(RealFileSystem fileSystem, CommandLine commandLine) throws IOException {
        String stringPath = CommandsServices.generateRealFilePath(fileSystem, commandLine, 1);
        BufferedReader file = new BufferedReader(new FileReader(stringPath));
        List<String> content = new ArrayList<>();
        String line;
        while ((line = file.readLine()) != null) {
            content.add(line);
        }

        try {
            UnsignedInt lineIndex = new UnsignedInt(Integer.parseInt(commandLine.getParameters().get(2)));
            if (lineIndex.getData() == 0) {
                lineIndex = new UnsignedInt(1);
            }
            String input = generateInput(commandLine, 3);
            content.set(lineIndex.getData() - 1, input);

            BufferedWriter fileOut = new BufferedWriter(new FileWriter(stringPath));
            for (String s : content) {
                fileOut.write(s + '\n');
            }
            fileOut.close();
            return CommandsServices.loadOutput("Successfully wrote into the file");
        }
        catch (IllegalArgumentException | IOException e) {
            throw new IllegalArgumentException();
        }
        finally {
            file.close();
        }
    }

    private List<String> overwriteIntoVirtualFileInCurrentDir(VirtualFileSystem fileSystem, CommandLine commandLine)
            throws FileNotFoundException {
        for (File f : fileSystem.getCurrentDir().getFiles()) {
            if (f.getName().equals(commandLine.getParameters().get(1))) {
                if (f.getClass() != TextFile.class) {
                    throw new IllegalArgumentException();
                }
                String input = generateInput(commandLine, 3);

                UnsignedInt line = new UnsignedInt(Integer.parseInt(commandLine.getParameters().get(2)));
                ((TextFile) f).overwrite(input, line);
                return CommandsServices.loadOutput("Successfully wrote into the file");

            }
        }
        throw new FileNotFoundException();
    }

    private List<String> writeIntoVirtualFileInRemoteDir(VirtualFileSystem fileSystem, CommandLine commandLine) throws IOException {
        String fileName = CommandsServices.generateFileName(commandLine, 0);
        Folder currentDir = fileSystem.getCurrentDir();
        CommandsServices.executeCd(fileSystem, commandLine, 0);

        for (File f : fileSystem.getCurrentDir().getFiles()) {
            if (f.getName().equals(fileName)) {
                if (f.getClass() != TextFile.class) {
                    throw new IllegalArgumentException();
                }
                String input = generateInput(commandLine, 2);
                try {
                    UnsignedInt line = new UnsignedInt(Integer.parseInt(commandLine.getParameters().get(1)));

                    ((TextFile) f).append(input, line);
                    fileSystem.setCurrentDir(currentDir);
                    return CommandsServices
                            .loadOutput("Successfully wrote into the file");
                }
                catch (IllegalArgumentException e) {
                    fileSystem.setCurrentDir(currentDir);
                    throw new IllegalArgumentException();
                }

            }
        }
        throw new FileNotFoundException();
    }

    private List<String> writeIntoRealFile(RealFileSystem fileSystem, CommandLine commandLine) throws IOException {
        String stringPath = commandLine.getParameters().get(0);
        if (!stringPath.contains("D:\\")) {
            stringPath = fileSystem.getCurrentDir().getPath() + "\\" + stringPath;
        }
        BufferedReader file = new BufferedReader(new FileReader(stringPath));
        List<String> content = new ArrayList<>();
        String line;
        while ((line = file.readLine()) != null) {
            content.add(line);
        }

        try {
            UnsignedInt lineIndex = new UnsignedInt(Integer.parseInt(commandLine.getParameters().get(1)));

            if (lineIndex.getData() == 0) {
                lineIndex = new UnsignedInt(1);
            }
            String input = generateInput(commandLine, 2);
            if (lineIndex.getData() <= content.size() - 1) {
                content.set(lineIndex.getData() - 1,content.get(lineIndex.getData() - 1) + input);
                BufferedWriter fileOut = new BufferedWriter(new FileWriter(stringPath));
                for (String s : content) {
                    fileOut.write(s + '\n');
                }
                fileOut.close();
                return CommandsServices.loadOutput("Successfully wrote into the file");
            }
            else  {
                BufferedWriter fileOut = new BufferedWriter(new FileWriter(stringPath, true));
                for (int i = content.size(); i < lineIndex.getData() - 1; i++) {
                    fileOut.write("" + '\n');
                }
                fileOut.write(input);
                fileOut.close();
                return CommandsServices.loadOutput("Successfully wrote into the file");
            }
        }
        catch (IllegalArgumentException e) {
            throw new IllegalArgumentException();
        }
        finally {
            file.close();
        }
    }

    private List<String> writeIntoVirtualFileInCurrentDir(VirtualFileSystem fileSystem, CommandLine commandLine) throws FileNotFoundException {
        for (File f : fileSystem.getCurrentDir().getFiles()) {
            if (f.getName().equals(commandLine.getParameters().get(0))) {
                if (f.getClass() != TextFile.class) {
                    throw new IllegalArgumentException();
                }
                String input = generateInput(commandLine, 2);
                UnsignedInt line = new UnsignedInt(Integer.parseInt(commandLine.getParameters().get(1)));
                ((TextFile) f).append(input, line);
                return CommandsServices.loadOutput("Successfully wrote into the file");
            }
        }
        throw new FileNotFoundException();
    }

}
