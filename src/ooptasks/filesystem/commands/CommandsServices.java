package ooptasks.filesystem.commands;

import ooptasks.filesystem.models.filesystems.FileSystem;
import ooptasks.filesystem.models.filesystems.RealFileSystem;
import ooptasks.filesystem.models.helpers.CommandLine;

import java.io.File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CommandsServices {

     static final char[] INVALID_CHARS = { '?', ' ', '>', '<', '`', '"',
            '#', ';', '%', '$', '@', '*', '^', '&'};

     static CommandLine generateCdCommandLine(String incompletePath) {
         String[] argumentWords = incompletePath.split("/");
         StringBuilder path = new StringBuilder();
         if (incompletePath.split(" ").length == 1 && !incompletePath.contains("/")) {
             CommandLine cdCommandLine = new CommandLine("cd");
             path.append(incompletePath);
             cdCommandLine.addParameter(path.toString());
             return cdCommandLine;
         }
         for (int i = 0; i < argumentWords.length - 1; i++) {
             if (argumentWords[i].equals("")) {
                 path.append("/");
             } else {
                 path.append(argumentWords[i]);
                 if (i < argumentWords.length - 2) {
                     path.append("/");
                 }
             }
         }
         CommandLine cdCommandLine = new CommandLine("cd");
         cdCommandLine.addParameter(path.toString());
         return cdCommandLine;
    }

    static String generateFileName(CommandLine commandLine, int index) {
        return commandLine
                .getParameters()
                .get(index)
                .split("/")[commandLine
                .getParameters()
                .get(index).split("/").length - 1];

    }

    static void executeCd(FileSystem fileSystem, CommandLine commandLine, int index) throws IOException {
             CommandLine cdCommandLine = generateCdCommandLine(commandLine.getParameters().get(index));
             FileSystem.commands.get(cdCommandLine.getCommand()).execute(fileSystem, cdCommandLine);
    }

    static List<String> loadOutput(String message) {
        List<String> output = new ArrayList<>();
        output.add(message);
        return output;
    }

    static double getRealFileSize(File file) {
        if (!file.isDirectory()) {
            return file.length();
        } else {
            double totalSize = 0;
            if (file.listFiles() != null) {
                for (File file1 : file.listFiles()) {
                    totalSize += getRealFileSize(file1);
                }
                return totalSize;
            }
            return 0;
        }
    }

    static String generateRealFilePath(RealFileSystem fileSystem, CommandLine commandLine, int indexOfPathParameter) {
        String stringPath = commandLine.getParameters().get(indexOfPathParameter);
        if (!stringPath.contains("D:\\")) {
            stringPath = fileSystem.getCurrentDir().getPath() + "\\" + stringPath;
        }
        return stringPath;
    }

}
