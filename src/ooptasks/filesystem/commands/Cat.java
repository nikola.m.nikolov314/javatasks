package ooptasks.filesystem.commands;
import ooptasks.filesystem.models.files.File;
import ooptasks.filesystem.models.files.Folder;
import ooptasks.filesystem.models.files.TextFile;
import ooptasks.filesystem.models.filesystems.FileSystem;
import ooptasks.filesystem.models.filesystems.RealFileSystem;
import ooptasks.filesystem.models.filesystems.VirtualFileSystem;
import ooptasks.filesystem.models.helpers.UnsignedInt;
import ooptasks.filesystem.models.helpers.CommandLine;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Cat implements Command {
    public List<String> execute(FileSystem fileSystem, CommandLine commandLine)
            throws IOException {
        validateInput(commandLine);
        if (commandLine.getParameters().get(0).contains("/") || commandLine.getParameters().get(0).contains("\\")) {
            return catFileInRemoteDir(fileSystem, commandLine);
        }
        return catFile(fileSystem, commandLine);
    }

    @Override
    public void validateInput(CommandLine commandLine) throws IllegalArgumentException{
        if (commandLine.getParameters().size() > 1
                || commandLine.getParameters().get(0).split(" ").length > 1) {
            throw new IllegalArgumentException();
        }

        for (char c : CommandsServices.INVALID_CHARS) {
            if (commandLine.getParameters().get(0).contains(Character.toString(c))) {
                throw new IllegalArgumentException();
            }
        }
    }

    private UnsignedInt getMaxKey(File f) {
        UnsignedInt maxKey = new UnsignedInt(0);
        for (UnsignedInt key : ((TextFile) f).getContent().keySet()) {
            if (key.isBigger(maxKey)) {
                maxKey = key;
            }
        }
        return maxKey;
    }

    private List<String> loadOutput(File f, UnsignedInt maxKey) {
        List<String> output = new ArrayList<>();
        for (int i = 1; i <= maxKey.getData(); i++) {
            output.add(((TextFile) f).getContent().getOrDefault(getKey(((TextFile) f).getContent(), i), ""));
        }
        return output;
    }

    private List<String> loadOutput(java.io.File f) throws IOException {
        List<String> output = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(f));
        String line;
        while ((line = br.readLine()) != null) {
            output.add(line);
        }
        return output;
    }

    private UnsignedInt getKey(Map<UnsignedInt, String> content, int i) {
        for (UnsignedInt ui : content.keySet()) {
            if (ui.getData() == i) {
                return ui;
            }
        }
        return null;
    }
    private List<String> catFileInRemoteDir(FileSystem fileSystem, CommandLine commandLine)
            throws IllegalArgumentException, IOException {
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return catVirtualFileInRemoteDir((VirtualFileSystem) fileSystem, commandLine);
        }
        return catRealFileInRemoteDir((RealFileSystem) fileSystem, commandLine);
    }

    private List<String> catFile(FileSystem fileSystem, CommandLine commandLine)
            throws IllegalArgumentException, IOException {
        if (fileSystem.getClass() == VirtualFileSystem.class) {
            return catVirtualFile((VirtualFileSystem) fileSystem, commandLine);
        }
        return catRealFile((RealFileSystem) fileSystem, commandLine);
    }

    private List<String> catVirtualFileInRemoteDir(VirtualFileSystem fileSystem, CommandLine commandLine) throws IOException {
        String fileName = CommandsServices.generateFileName(commandLine, 0);
        Folder currentDir = fileSystem.getCurrentDir();
        CommandsServices.executeCd(fileSystem, commandLine, 0);
        for (File f :  fileSystem.getCurrentDir().getFiles()) {
            if (f.getName().equals(fileName)) {
                if (f.getClass() != TextFile.class) {
                    fileSystem.setCurrentDir(currentDir);
                    throw new IllegalArgumentException();
                }
                UnsignedInt maxKey = getMaxKey(f);
                List<String> output = loadOutput(f, maxKey);
                fileSystem.setCurrentDir(currentDir);
                return output;
            }
        }
        fileSystem.setCurrentDir(currentDir);
        throw new FileNotFoundException();
    }

    private List<String> catRealFileInRemoteDir(RealFileSystem fileSystem, CommandLine commandLine) throws IOException {
        String stringPath = commandLine.getParameters().get(0).substring(0, commandLine.getParameters().get(0).lastIndexOf('\\'));
        if (!stringPath.contains("D:\\")) {
            stringPath = fileSystem.getCurrentDir().getPath() + "\\" + stringPath;
        }
        Path path = Paths.get(stringPath);
        String fileName = commandLine.getParameters().get(0).substring(commandLine.getParameters().get(0).lastIndexOf('\\') + 1);
        java.io.File[] files = path.toFile().listFiles();
        if (files != null) {
            for (java.io.File f : files) {
                if (f.getName().equals(fileName)) {
                    return loadOutput(f);
                }
            }
        }
        throw new FileNotFoundException();
    }

    private List<String> catVirtualFile(VirtualFileSystem fileSystem, CommandLine commandLine) throws FileNotFoundException {
        for (File f : fileSystem.getCurrentDir().getFiles()) {
            if (f.getName().equals(commandLine.getParameters().get(0))) {
                if (f.getClass() != TextFile.class) {
                    throw new IllegalArgumentException();
                }
                UnsignedInt maxKey = getMaxKey(f);
                return loadOutput(f, maxKey);
            }
        }
        throw new FileNotFoundException();
    }

    private List<String> catRealFile(RealFileSystem fileSystem, CommandLine commandLine) throws IOException {
        String fileName = commandLine.getParameters().get(0);

        java.io.File[] files = fileSystem.getCurrentDir().listFiles();
        if (files != null) {
            for (java.io.File f : files) {
                if (f.getName().equals(fileName)) {
                    return loadOutput(f);
                }
            }
        }
        throw new FileNotFoundException();
    }
}

