package streamstasks;

public class Student {
    private double mark;
    private String name;
    private char gender;
    private int age;

    public Student(String name, int age, char gender, double mark) {
        setMark(mark);
        setAge(age);
        setGender(gender);
        this.name = name;
    }

    public void setName(String name) { this.name = name; }

    public String getName() { return name; }

    public void setMark(double mark) {
        if (mark < 2 || mark > 6) {
            this.mark = 2;
        }
        else this.mark = mark;
    }

    public double getMark() { return  mark; }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public char getGender() { return gender; }

    public void setAge(int age) {
        if (age < 0) {
            this.age = 10;
        }
        else this.age = age;
    }

    public int getAge() { return age; }

}
