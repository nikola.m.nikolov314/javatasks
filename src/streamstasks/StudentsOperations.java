package streamstasks;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class StudentsOperations {

     static OptionalDouble getAverageMark(List<Student> students) {
        OptionalDouble average = students.stream().mapToDouble(Student::getMark).average();
        return average;
    }

    static List<Student> getPassingStudents(List<Student> students, double minMarkRequired) {
         return students.stream().filter(student -> student.getMark() >= minMarkRequired).collect(toList());
    }

    static List<Student> getFailingStudents(List<Student> students, double minMarkRequired) {
        return students.stream().filter(student -> student.getMark() < minMarkRequired).collect(toList());
    }

    static Map<Boolean, List<Student>> splitStudents(double mark, List<Student> students) {
         return students.stream().collect(Collectors.groupingBy(student -> student.getMark() > mark));
    }

    static List<Student> orderStudentsAsc(List<Student> students) {
         return students.stream().sorted(Comparator.comparing(Student::getMark)).collect(toList());
    }

    static List<Student> orderStudentsDesc(List<Student> students) {
        return students.stream().sorted(Comparator.comparing(Student::getMark).reversed()).collect(toList());
    }

    static List<Student> clusterizeAndGetHighestGrade(List<Student> students) {
         Map<Double, List<Student>> clusters = students.stream().collect(Collectors.groupingBy(Student::getMark));
         double highestKey = clusters.keySet().stream().max(Double::compareTo).orElse((double) -1);
         return clusters.get(highestKey);
    }

    static List<Student> clusterizeAndGetLowestGrade(List<Student> students) {
        Map<Double, List<Student>> clusters = students.stream().collect(Collectors.groupingBy(Student::getMark));
        double highestKey = clusters.keySet().stream().min(Double::compareTo).orElse((double) -1);
        return clusters.get(highestKey);
    }

    static Map<Integer, List<Double>> getMarksByAge(List<Student> students) {
         return students.stream().collect(Collectors.groupingBy(Student::getAge,
                 Collectors.mapping(Student::getMark, Collectors.toList())));
    }

    static Map<Character, Double> getAverageByGender(List<Student> students) {
        return students.stream().collect(Collectors.groupingBy(Student::getGender,
                Collectors.averagingDouble(Student::getMark)));
    }

    static Map<Character, Map<Integer, List<Student>>> splitAndPartition(List<Student> students) {
        return students.stream().collect(Collectors.groupingBy(Student::getGender,
                Collectors.groupingBy(Student::getAge)));
    }

    public static  void main(String[] args) {
        List<Student> students = Arrays.asList(new Student("Gosho", 20, 'M',  2),
                new Student("Pesho",15, 'M',  4),
                new Student("Mariq", 20, 'F',  5),
                new Student("Petkan", 20, 'M',  3));
        var a = splitAndPartition(students);

        for (Student s : a.get('M').get(20)) {
            System.out.println(s.getName());
        }

    }
}
