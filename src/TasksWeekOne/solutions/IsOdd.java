package tasksweekone.solutions;

public class IsOdd {
    public static boolean isOdd(int number) {
        if (number % 2 == 0) {
            return  false;
        }
        return true;
    }
}
