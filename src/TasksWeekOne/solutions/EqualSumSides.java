package tasksweekone.solutions;

public class EqualSumSides {
    public static boolean equalSumSides(int[] numbers) {
        if (numbers.length == 0) {
            return false;
        }
        for (int i = 0; i < numbers.length; i++) {
            int sum1 = 0;
            int sum2 = 0;
            for (int j = 0; j < i; j++) {
                sum1 += numbers[j];
            }

            for (int j = i + 1; j < numbers.length; j++) {
                sum2 += numbers[j];
            }

            if (sum1 == sum2) {
                return true;
            }
        }
        return false;
    }

}
