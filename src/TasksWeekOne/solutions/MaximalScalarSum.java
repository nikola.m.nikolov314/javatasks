package tasksweekone.solutions;

public class MaximalScalarSum {
    public static long maximalScalarSum(int[] a, int[] b) {
        if (a.length != b.length) {
            return 0;
        }
        for (int i = 0; i < a.length - 1; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (a[j] < a[i]) {
                    int tmp = a[j];
                    a[j] = a[i];
                    a[i] = tmp;
                }
            }
        }

        for (int i = 0; i < b.length - 1; i++) {
            for (int j = i + 1; j < b.length; j++) {
                if (b[j] < b[i]) {
                    int tmp = b[j];
                    b[j] = b[i];
                    b[i] = tmp;
                }
            }
        }

        int result = 0;
        for (int i = 0; i < a.length; i++) {
            result += a[i] * b[i];
        }
        return result;
    }
}
