package tasksweekone.solutions;

public class MaxSpan {
    public static int maxSpan(int[] numbers) {
        if (numbers.length == 0) {
            return 0;
        }
        int maxSpan = 0;
        for (int i = 0; i < numbers.length; i++) {
            for (int j = numbers.length - 1; j >= 0; j--) {
                if (numbers[i] == numbers[j]) {
                    int span = j - i - 1;
                    if (span > maxSpan) {
                        maxSpan = span;
                    }
                }
            }
        }
        return maxSpan;

    }

}
