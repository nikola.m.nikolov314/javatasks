package tasksweekone.solutions;

import static tasksweekone.solutions.IsAnagram.isAnagram;

public class HasAnagramOf {
    public static boolean hasAnagramOf(String A, String B) {
        String[] stringArray = A.split(" ");
        for (String s : stringArray) {
            if (isAnagram(s, B)) {
                return true;
            }
        }
        return false;
    }
}
