package tasksweekone.solutions;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static java.awt.image.BufferedImage.TYPE_BYTE_INDEXED;

public class GreyScale {

    public static void greyScale(String path) {
            try {
                BufferedImage image =  ImageIO.read(new File(path));
                System.out.println(image.getHeight());
                System.out.println(image.getWidth());
                BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), TYPE_BYTE_INDEXED);



                for (int i  = 0; i < image.getHeight(); i++) {
                    for (int j = 0; j < image.getWidth(); j++) {
                        int color = image.getRGB(j, i);
                        int alpha = (color >> 24) & 255;
                        int red = (color >> 16) & 255;
                        int green = (color >> 8) & 255;
                        int blue = color & 255;
                        int average = (red + blue + green) / 3;

                        int newColor = (alpha << 24) | (average << 16) | (average << 8) | average;

                        image.setRGB(j, i, newColor);

                    }

                }
                File f = new File(path);
                ImageIO.write(image, path, f);
            }
            catch (IOException e) {
                System.out.println("gre6ka!");
            }

        }

    }

