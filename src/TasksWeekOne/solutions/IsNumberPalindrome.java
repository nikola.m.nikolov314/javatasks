package tasksweekone.solutions;

import static tasksweekone.solutions.IsPalindrome.isPalindrome;

public class IsNumberPalindrome {
    public static boolean isNumberPalindrome(int number) {
        String s = Integer.toString(number);
        return isPalindrome(s);
    }
}
