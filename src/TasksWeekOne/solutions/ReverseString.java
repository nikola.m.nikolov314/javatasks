package tasksweekone.solutions;

public class ReverseString {
    public static String reverseString(String s) {
        StringBuilder s1 = new StringBuilder(s);
        s1.reverse();
        return s1.toString();

    }
}
