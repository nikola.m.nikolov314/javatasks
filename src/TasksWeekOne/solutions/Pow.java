package tasksweekone.solutions;


import tasksweekone.exceptions.InvalidPowerException;

public class Pow {
    public static long pow(int a, int b) throws InvalidPowerException {

        if (b < 0) {
            InvalidPowerException invalidPowerExc = new InvalidPowerException("Invalid power");
            throw invalidPowerExc;
        }

        if(b == 0) {
            return 1;
        }
        else if(b == 1) {
            return (long) a;
        }

        int binary_digits_of_b[] = new int[33];

        int i = 0;

        while(b > 0) {
            if(b % 2 == 1) {
                binary_digits_of_b[i] = 1;
            }
            b = b/2;
            i++;
        }

        int max_power_of_2 = 0;
        for (int k = 0; k < binary_digits_of_b.length; k++) {
            if (binary_digits_of_b[k] == 1) {
                max_power_of_2 = k;
            }
        }

        int powers_of_a[] = new int[max_power_of_2 + 1];

        powers_of_a[0] = a;

        for(int j = 1; j <= max_power_of_2; j++) {
            powers_of_a[j] = powers_of_a[j-1] * powers_of_a[j-1];
        }

        long result = 1;

        for(int k = 0; k <= max_power_of_2; k++) {
            if(binary_digits_of_b[k] == 1) {
                System.out.println(powers_of_a[k]);
                result *= powers_of_a[k];
            }
        }
        return result;
    }
}
