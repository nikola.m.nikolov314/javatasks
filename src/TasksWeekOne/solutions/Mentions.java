package tasksweekone.solutions;

public class Mentions {
    public static int mentions(String input, String word) {
        String s = input.replaceAll(word, "*");
        char[] helpArr = s.toCharArray();
        int counter = 0;
        for (char a : helpArr) {
            if (a == '*') {
                counter++;
            }
        }
        return counter;
    }

}
