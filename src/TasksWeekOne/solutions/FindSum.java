package tasksweekone.solutions;

public class FindSum {
    public static int findSum(String input) {
        var newInput = input.replaceAll("[^0-9-]+", "*");
        var newInput1 = newInput.split("\\*");
        int sum = 0;
        System.out.println(newInput);


        for (var a : newInput1) {
            if (a.isEmpty()) {
                continue;
            }
            sum += Integer.parseInt(a);
        }

        return sum;
    }
}
