package tasksweekone.solutions;

import java.util.Arrays;
import java.util.Collections;
import java.util.NoSuchElementException;

public class GetOddOccurrence {
    public static int getOddOccurrence(int[] array) throws NoSuchElementException {
        Arrays.sort(array);
        int stepsCounter = 1;
        for (int i = 0; i < array.length - 1; i += stepsCounter) {
            int numberCounter = 1;
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    numberCounter++;
                }
            }
            if (numberCounter % 2 != 0) {
                return array[i];
            }
            stepsCounter = numberCounter;
        }
        throw new NoSuchElementException();
    }
}
