package tasksweekone.solutions;

import static tasksweekone.solutions.ReverseString.reverseString;

public class ReverseEachWord {
    public static String reverseEachWord(String s) {
        String[] a = s.split(" ");
        for (int i = 0; i < a.length; i++) {
            a[i] = reverseString(a[i]);
        }
        String b = String.join(" ", a);
        return b;
    }
}
