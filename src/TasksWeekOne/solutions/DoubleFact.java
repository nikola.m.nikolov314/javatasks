package tasksweekone.solutions;

public class DoubleFact {
    public static long doubleFact(int n) {
        if (n == 0) {
            return 1;
        }
        int result = 1;
        for (int i = 0; i < 2; i++) {
            result = 1;
            for (int j = n; j >= 1; j--) {
                result *= j;
            }
            n = result;
        }
        return result;
    }

}
