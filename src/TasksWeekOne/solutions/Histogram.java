package tasksweekone.solutions;

public class Histogram {
    public static int[] histogram(int[][] image) {
        int maxNumber = 0;

        for (int row = 0; row < image.length; row++) {
            for (int col = 0; col < image[row].length; col++) {
                if (image[row][col] > maxNumber) {
                    maxNumber = image[row][col];
                }
            }
        }

        int[] result = new int[maxNumber + 1];

        for (int row = 0; row < image.length; row++) {
            for (int col = 0; col < image[row].length; col++) {
                result[image[row][col]]++;
            }
        }
        return result;
    }
}
