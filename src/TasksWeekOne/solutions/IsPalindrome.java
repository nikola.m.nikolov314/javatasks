package tasksweekone.solutions;

import static tasksweekone.solutions.ReverseString.reverseString;

public class IsPalindrome {
    public static boolean isPalindrome(String s) {
        String s1 = reverseString(s);
        return s1.equals(s);
    }
}
