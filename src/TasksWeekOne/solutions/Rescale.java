package tasksweekone.solutions;

public class Rescale {
    public static int[][] rescale(int[][] original, int newHeight, int newWidth) {
        int[][] result = new int[newHeight][newWidth];

        double proportion = (double) original.length / newHeight;

        for (int i = 0; i < newHeight; i++) {
            for (int j = 0; j < newWidth; j++) {
                int newRowsIndex = (int) Math.round(i * proportion);
                int newColsIndex = (int) Math.round(j * proportion);

                if (newRowsIndex >= original.length) {
                    newRowsIndex--;
                }

                if (newColsIndex >= original.length) {
                    newColsIndex--;
                }

                result[i][j] = original[newRowsIndex][newColsIndex];
            }
        }

        int[][] interpolatedMatrix = new int[newHeight][newWidth];
        int[][] moves = {{-1,0}, {-1,1}, {0,1}, {1, 1}, {1, 0}, {1, -1}, {0,-1}, {-1,-1}};
        int sum = 0;
        int count = 1;
        for (int i = 0; i < newHeight; i++) {
            for (int j = 0; j < newWidth; j++) {
                sum = result[i][j];

                for(int k = 0; k < moves.length; k++){
                    if(isValidMove(i + moves[k][0], j + moves[k][1], newHeight, newWidth)){
                        sum += result[i + moves[k][0]][j + moves[k][1]];
                        count++;
                    }
                }
                interpolatedMatrix[i][j] = (int)Math.round(sum / count);
            }
        }
        return interpolatedMatrix;
    }

    private static boolean isValidMove(int i, int j, int height, int width){
        return i < height && i >= 0 && j < width && j >= 0;
    }
}
