package tasksweekone.solutions;

public class GetAverage {
    public static int getAverage(int[] array) {
        if (array.length == 0) {
            return 0;
        }
        int sum = 0;
        for (int element : array) {
            sum += element;
        }
        return sum / array.length;
    }
}
