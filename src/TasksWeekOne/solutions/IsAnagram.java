package tasksweekone.solutions;

public class IsAnagram {
    public static boolean isAnagram(String A, String B) {
        String s = A.toLowerCase();
        String s1 = B.toLowerCase();
        char[] a = s1.toCharArray();
        char[] b = s.toCharArray();

        if (a.length != b.length) {
            return false;
        }

        for (int i = 0; i < a.length - 1; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (a[i] > a[j]) {
                    char tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;
                }
            }
        }

        for (int i = 0; i < b.length - 1; i++) {
            for (int j = i + 1; j < b.length; j++) {
                if (b[i] > b[j]) {
                    char tmp = b[i];
                    b[i] = b[j];
                    b[j] = tmp;
                }
            }
        }

        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }

        return true;

    }
}
