package tasksweekone.solutions;

import static tasksweekone.solutions.IsNumberPalindrome.isNumberPalindrome;

public class GetLargestPalindrome {
    public static long getLargestPalindrome(long N) {
        long maxNumber = 0;
        for (long i = 0; i <= N; i++) {
            if (isNumberPalindrome((int) i) && i > maxNumber) {
                maxNumber = i;
            }
        }
        return maxNumber;
    }
}
