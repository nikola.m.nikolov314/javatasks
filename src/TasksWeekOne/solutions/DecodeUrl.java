package tasksweekone.solutions;

public class DecodeUrl {
    public static String decodeUrl(String input) {
        String result;
        result = input.replaceAll("%20", " ");
        result = result.replaceAll("%3A", ":");
        result = result.replaceAll("%3D", "?");
        result = result.replaceAll("%2F", "/");
        return result;
    }
}
