package tasksweekone.solutions;

import java.util.NoSuchElementException;

public class KthMin {
    public static int kthMin(int k, int[] numbers) throws ArrayIndexOutOfBoundsException, NoSuchElementException {
        if (numbers.length == 0) {
            throw new NoSuchElementException("the array is empty");
        }

        if (k < 0 || k >= numbers.length) {
            throw new ArrayIndexOutOfBoundsException("Out of index");
        }

        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = i + 1; j < numbers.length; j++) {
                if (numbers[j] < numbers[i]) {
                    int tmp = numbers[j];
                    numbers[j] = numbers[i];
                    numbers[i] = tmp;
                }
            }
        }
        return numbers[k];
    }
}
