package tasksweekone.solutions;

public class KthFact {
    public static long kthFact(int n, int k) {
        if (n == 0 || n == 1) {
            return 1;
        }
        int result = 1;
        for (int i = 0; i < k; i++) {
            result = 1;
            for (int j = n; j >= 1; j--) {
                result *= j;
            }
            n = result;
        }
        return result;
    }
}
