package tasksweekone.exceptions;

public class InvalidPowerException extends Exception {
    public InvalidPowerException() {
    }

    public InvalidPowerException(String message) {
        super(message);
    }
}
