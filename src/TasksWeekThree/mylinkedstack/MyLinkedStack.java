package tasksweekthree.mylinkedstack;

public class MyLinkedStack {
    private Node top;
    public MyLinkedStack() {
        top = null;
    }

    public Node getTop() { return this.top; }
    public void setTop(Node n) { this.top = n; }

    public void push(int data) {
        Node newTop = new Node(data, this.getTop());
        this.setTop(newTop);
    }

    public int peek() {
        if (this.getTop() == null) {
            return -5;
        }

        return this.getTop().getData();
    }

    public boolean pop() {
        if (this.getTop() == null) {
            return false;
        }

        this.setTop(this.getTop().getNext());
        return true;
    }

    public boolean isEmpty() {
        return this.getTop() == null;
    }

}

