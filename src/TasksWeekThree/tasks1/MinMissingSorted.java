package tasksweekthree.tasks1;

public class MinMissingSorted {
    public static int solve(int[] arr, int n) {
        int left = 0;
        int right = arr.length - 1;

        if (arr.length == 0) {
            return 1;
        }

        if (arr.length == 1) {
            if (arr[0] != 1) {
                return 1;
            }
            else return 2;
        }

        while(left < right) {
            int mid = (right + left) / 2;
            if (right - left == 1) {
                if (arr[right] - right != 1) {
                    return right + 1;
                }
                return left - 1;
            }
            if (arr[mid] - mid != 1) {
                right = mid;
            }
            else left = mid;
        }
        return -5;
    }
}
