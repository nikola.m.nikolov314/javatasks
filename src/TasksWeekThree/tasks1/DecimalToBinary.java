package tasksweekthree.tasks1;

import java.util.Stack;

public class DecimalToBinary {
    public static int[] solve(int n) {
        Stack<Integer> s = new Stack<>();

        while (n > 0) {
            if (n % 2 != 0) {
                s.push(1);
            }
            else s.push(0);
            n /= 2;
        }
        int[] ans = new int[s.size()];
        int index = 0;
        while(!s.isEmpty()) {
            ans[index++] = s.pop();
        }
        return ans;
    }
}
