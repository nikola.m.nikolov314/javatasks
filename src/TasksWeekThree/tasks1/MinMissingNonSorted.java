package tasksweekthree.tasks1;

public class MinMissingNonSorted {
    public static int solve(int[] arr, int n) {
        boolean[] values = new boolean[n + 1];

        for (int i = 0; i < values.length; i++) {
            values[i] = false;
        }

        for (int i = 0; i < arr.length; i++) {
            values[arr[i] - 1] = true;
        }

        for (int i = 0; i < values.length; i++) {
            if (!values[i]) {
                return i + 1;
            }
        }
        return -5;
    }
}
