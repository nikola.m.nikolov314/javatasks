package tasksweekthree.tasks1;

import java.util.Stack;

public class BracketsCorrectedness {
    public static boolean solve(String expression) {
        char[] arr = expression.toCharArray();
        Stack<Character> s = new Stack<Character>();

        for (char c : arr) {
            if (c == '(') {
                s.push(c);
            }
            else if (c == ')') {
                if (s.isEmpty()) {
                    return false;
                }
                s.pop();
            }
        }
        if (!s.isEmpty()) {
            return false;
        }
        return true;
    }
}
