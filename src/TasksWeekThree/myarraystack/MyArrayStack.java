package tasksweekthree.myarraystack;

import java.util.Collections;

public class MyArrayStack {

    private int[] elements;
    private int size;
    private int capacity;

    public MyArrayStack() {
        this.elements = new int[1];
        this.size = 0;
        this.capacity = 1;
    }

    public int[] getElements() {
        return this.elements;
    }

    public void setElements(int[] elements) {
        this.elements = elements;
    }

    public int getSize() {
        return this.size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getCapacity() {
        return this.capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    private void resize() {
        int[] newElements = new int[this.getCapacity() * 2];

        for (int i = 0; i < this.getSize(); i++) {
            newElements[i] = this.getElements()[i];
        }
        this.setElements(newElements);
        this.setCapacity(this.getCapacity() * 2);
    }

    public void push(int element) {

        if (this.getSize() >= this.getCapacity()) {
            this.resize();
        }

        this.elements[this.getSize()] = element;
        this.setSize(this.getSize() + 1);
    }

    public boolean pop() {
        if (this.getSize() == 0) {
            return false;
        }

        this.setSize(this.getSize() - 1);
        return true;
    }

    public int peek() {
        if (this.getSize() == 0) {
            return -5;
        }
        return this.getElements()[this.getSize() - 1];
    }

    public boolean isEmpty() {
        return this.getSize() == 0;
    }
}
