package tasksweekthree.myqueue;

import java.util.Stack;

public class MyQueue {
    private int[] elements;
    private int capacity;
    private int size;
    private int firstIndex;
    private int lastIndex;

    public MyQueue() {
        this.elements = new int[1];
        this.size = 0;
        this.capacity = 1;
        this.firstIndex = 0;
        this.lastIndex = 0;
    }

    public int[] getElements() {
        return this.elements;
    }

    public void setElements(int[] elements) {
        this.elements = elements;
    }

    public int getSize() {
        return this.size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getCapacity() {
        return this.capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getFirstIndex() {
        return firstIndex;
    }

    public void setFirstIndex(int index) {
        this.firstIndex = index;
    }

    public int getLastIndex() {
        return lastIndex;
    }

    public void setLastIndex(int lastIndex) {
        this.lastIndex = lastIndex;
    }

    private void resize() {
        int[] newElements = new int[this.getCapacity() * 2];
        int newElsIndex = 0;

        if (firstIndex == lastIndex) {
            newElements[0] = this.getElements()[0];
        }
        else if (firstIndex > lastIndex) {
            for (int i = firstIndex; i < this.getSize(); i++) {
                newElements[newElsIndex++] = this.getElements()[i];
            }

            for (int i = 0; i <= lastIndex; i++) {
                newElements[newElsIndex++] = this.getElements()[i];
            }
        }
        else if (firstIndex < lastIndex) {
            for (int i = 0; i < this.getSize(); i++) {
                newElements[i] = this.getElements()[i];
            }
        }
        this.setFirstIndex(0);
        this.setLastIndex(this.getSize() - 1);
        this.setElements(newElements);
        this.setCapacity(this.getCapacity() * 2);
    }

    public void enqueue(int element) {
        if (this.getSize() >= this.getCapacity()) {
            this.resize();
        }

        if (this.getSize() == 0) {
            this.getElements()[0] = element;
            this.setSize(this.getSize() + 1);
        }

        else if (firstIndex == lastIndex) {
            this.getElements()[++lastIndex] = element;
            this.setSize(this.getSize() + 1);
        }
        else if (firstIndex <= lastIndex) {
            if (lastIndex < this.getCapacity() - 1) {
                this.getElements()[++lastIndex] = element;
                this.setSize(this.getSize() + 1);
            }
            else {
                this.getElements()[0] = element;
                this.setSize(this.getSize() + 1);
                this.setLastIndex(0);
            }
        }
        else if (lastIndex < firstIndex) {
            this.getElements()[++lastIndex] = element;
            this.setSize(this.getSize() + 1);
        }

    }

    public boolean dequeue() {
        if (this.getSize() == 0) {
            return false;
        }

        firstIndex++;
        this.setSize(this.getSize() - 1);
        return true;
    }

    public boolean isEmpty() {
        return this.getSize() == 0;
    }

    public int front() {
        if (this.getSize() == 0) {
            return -5;
        }
        return this.getElements()[firstIndex];
    }

}
