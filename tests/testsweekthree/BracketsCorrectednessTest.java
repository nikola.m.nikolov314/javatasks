package testsweekthree;

import org.junit.jupiter.api.Test;
import tasksweekthree.tasks1.BracketsCorrectedness;

import static org.junit.jupiter.api.Assertions.*;

class BracketsCorrectednessTest {

    @Test
    void ReturnTrueForValidExpression() {
        assertTrue(BracketsCorrectedness.solve("((()))"));
        assertTrue(BracketsCorrectedness.solve("()()()"));
        assertTrue(BracketsCorrectedness.solve("()"));
        assertTrue(BracketsCorrectedness.solve("()(())"));
        assertTrue(BracketsCorrectedness.solve("(()())"));
    }

    @Test
    void ReturnFalseForInvalidExpression() {
        assertFalse(BracketsCorrectedness.solve(")("));

        assertFalse(BracketsCorrectedness.solve(")"));
        assertFalse(BracketsCorrectedness.solve("("));
        assertFalse(BracketsCorrectedness.solve(")()("));
    }
}