package testsweekthree;

import org.junit.jupiter.api.Test;
import tasksweekthree.linkedlist.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

class LinkedListTest {

    @Test
    void SucceedToAddElementAtTheFirstPositionInNonEmptyList() {
        LinkedList l = new LinkedList();
        l.add(1, 1);
        l.add(2, 2);
        assertTrue(l.add(0, 5));
        assertEquals(l.get(0), 5);
        assertTrue(l.add(0, 2));
        assertEquals(l.get(0), 2);
        assertTrue(l.add(0, 7));
        assertEquals(l.get(0), 7);
        assertTrue(l.add(0, 8));
        assertEquals(l.get(0), 8);

    }

    @Test
    void SucceedToAddElementAtTheFirstPositionEmptyList() {
        LinkedList l = new LinkedList();
        assertTrue(l.add(0, 5));
        assertEquals(l.get(0), 5);
        assertTrue(l.add(0, 2));
        assertEquals(l.get(0), 2);
        assertTrue(l.add(0, 7));
        assertEquals(l.get(0), 7);
        assertTrue(l.add(0, 8));
        assertEquals(l.get(0), 8);

    }

    @Test
    void SucceedToAddElementAtTheLastPositionInNonEmptyList() {
        LinkedList l = new LinkedList();
        l.add(0, 1);
        l.add(1, 2);

        assertTrue(l.add(l.getSize(), 3));
        assertEquals(3, l.get(l.getSize() - 1));

        assertTrue(l.add(l.getSize(), 5));
        assertEquals(5, l.get(l.getSize() - 1));

        assertTrue(l.add(l.getSize(), 7));
        assertEquals(7, l.get(l.getSize() - 1));
    }

    @Test
    void SucceedToAddElementAtMidPositionInNonEmptyList() {
        LinkedList l = new LinkedList();
        l.add(0, 1);
        l.add(1, 2);
        l.add(2, 3);

        assertTrue(l.add(1, 4));
        assertEquals(4, l.get(1));

        assertTrue(l.add(2, 8));
        assertEquals(8, l.get(2));

        assertTrue(l.add(3, 16));
        assertEquals(16, l.get(3));

    }


    @Test
    void FailToAddElementAtNegativeIndex() {
        LinkedList l = new LinkedList();
        assertFalse(l.add(-1, 2));
        assertFalse(l.add(-4, 2));
        assertFalse(l.add(-3, 2));
        assertFalse(l.add(-5, 2));
    }

    @Test
    void FailToAddElementAtIndexBiggerThanSize() {
        LinkedList l = new LinkedList();
        l.add(0, 0);
        l.add(1, 1);
        l.add(2, 2);
        assertFalse(l.add(4, 2));
        assertFalse(l.add(5, 2));
        assertFalse(l.add(7, 2));
        assertFalse(l.add(12, 2));
    }
}