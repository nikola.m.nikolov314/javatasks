package testsweekthree;

import org.junit.jupiter.api.Test;
import tasksweekthree.myarraystack.MyArrayStack;

import static org.junit.jupiter.api.Assertions.*;

class MyArrayStackTest {
    MyArrayStack ms = new MyArrayStack();

    @Test
    void PushElementIntoAnEmptyArrayStack() {
        ms.push(1);
        assertEquals(ms.peek(), 1);
    }

    @Test
    void PushElementIntoNonEmptyArrayStack() {
        ms.push(2);
        assertEquals(ms.peek(), 2);

        ms.push(3);
        assertEquals(ms.peek(), 3);

        ms.push(3);
        assertEquals(ms.peek(), 3);

        ms.push(4);
        assertEquals(ms.peek(), 4);
    }

    @Test
    void FailToPopFromEmptyStack() {
        MyArrayStack ms1 = new MyArrayStack();
        assertFalse(ms1.pop());
    }

    @Test
    void SucceedToPopFromNonEmptyStack() {
        MyArrayStack ms1 = new MyArrayStack();
        ms1.push(1);
        ms1.push(2);
        ms1.push(3);
        ms1.push(4);
        assertTrue(ms1.pop());
        assertEquals(3, ms1.peek());
        assertTrue(ms1.pop());
        assertEquals(2, ms1.peek());
        assertTrue(ms1.pop());
        assertEquals(1, ms1.peek());
    }
}