package testsweekthree;

import org.junit.jupiter.api.Test;
import tasksweekthree.tasks1.DecimalToBinary;

import static org.junit.jupiter.api.Assertions.*;

class DecimalToBinaryTest {

    @Test
    void SucceedToConvertDecimalToBinary() {
        int[] numbers = {1, 0, 1, 0};
        assertArrayEquals(numbers, DecimalToBinary.solve(10));
    }
}