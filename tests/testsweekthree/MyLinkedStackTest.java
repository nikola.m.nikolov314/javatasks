package testsweekthree;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tasksweekthree.mylinkedstack.MyLinkedStack;

import static org.junit.jupiter.api.Assertions.*;

class MyLinkedStackTest {
    MyLinkedStack ls = new MyLinkedStack();
    @Test
    void SucceedToPushElementIntoEmptyStack() {

        ls.push(0);
        Assertions.assertEquals(0, ls.peek());

        ls.push(1);
        Assertions.assertEquals(1, ls.peek());

        ls.push(2);
        Assertions.assertEquals(2, ls.peek());

        ls.push(3);
        Assertions.assertEquals(3, ls.peek());
    }

}