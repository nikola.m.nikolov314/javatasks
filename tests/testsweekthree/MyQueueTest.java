package testsweekthree;

import org.junit.jupiter.api.Test;
import tasksweekthree.myqueue.MyQueue;

import static org.junit.jupiter.api.Assertions.*;

class MyQueueTest {

    MyQueue aq = new MyQueue();

    @Test
    public void Enqueue_Succeed_AddElementAtEmptyQueue() {
        assertEquals(0, aq.getSize());

        aq.enqueue(3);

        assertEquals(3, aq.getElements()[0]);
        assertEquals(1, aq.getSize());
    }

    @Test
    public void Enqueue_Succeed_AddElementAtNotEmptyQueue() {
        aq.enqueue(3);

        aq.enqueue(5);
        aq.enqueue(6);
        aq.enqueue(7);
        aq.enqueue(8);

        assertEquals(5, aq.getSize());

        assertEquals(8, aq.getCapacity());

        assertEquals(5, aq.getElements()[1]);
        assertEquals(6, aq.getElements()[2]);
        assertEquals(7, aq.getElements()[3]);
        assertEquals(8, aq.getElements()[4]);
    }

    @Test
    public void Dequeue_Fail_RemoveElementFromEmptyQueue() {
        assertFalse(aq.dequeue());
    }

    @Test
    public void Dequeue_Succeed_RemoveElementFromNotEmptyQueue() {
        aq.enqueue(3);
        aq.enqueue(5);
        aq.enqueue(6);
        aq.enqueue(7);
        aq.enqueue(8);

        assertTrue(aq.dequeue());
        assertTrue(aq.dequeue());

        assertEquals(3, aq.getSize());

        assertTrue(aq.dequeue());
        assertTrue(aq.dequeue());
        assertTrue(aq.dequeue());

        assertEquals(0, aq.getSize());
    }


    @Test
    public void Enqueue_Succeed_EnqueueAfterDeqeueue() {
        aq.enqueue(3);
        aq.enqueue(5);
        aq.enqueue(6);
        aq.enqueue(7);
        aq.enqueue(8);

        aq.dequeue();
        aq.dequeue();
        aq.dequeue();

        aq.enqueue(9);
        aq.enqueue(10);
        aq.enqueue(11);

        // Adding at first position

        aq.enqueue(2);

        assertEquals(2, aq.getElements()[0]);
        assertEquals(7, aq.getElements()[3]);
        assertEquals(8, aq.getElements()[4]);
        assertEquals(9, aq.getElements()[5]);
        assertEquals(10, aq.getElements()[6]);
        assertEquals(11, aq.getElements()[7]);

        assertEquals(0, aq.getLastIndex());
        assertEquals(3, aq.getFirstIndex());

        aq.enqueue(3);
        aq.enqueue(4);

        assertEquals(3, aq.getElements()[1]);
        assertEquals(4, aq.getElements()[2]);

        aq.enqueue(21);

        assertEquals(7, aq.getElements()[0]);
        assertEquals(8, aq.getElements()[1]);
        assertEquals(9, aq.getElements()[2]);
        assertEquals(10, aq.getElements()[3]);
        assertEquals(11, aq.getElements()[4]);
        assertEquals(2, aq.getElements()[5]);
        assertEquals(3, aq.getElements()[6]);
        assertEquals(4, aq.getElements()[7]);
        assertEquals(21, aq.getElements()[8]);

        aq.enqueue(11);

        assertEquals(11, aq.getElements()[9]);

    }

    @Test
    public void Front_Fail_FrontFromEmptyQueue() {
        assertEquals(-5, aq.front());
    }

    @Test
    public void Front_Succeed_FrontFromNotEmptyQueue() {
        aq.enqueue(3);
        aq.enqueue(5);
        aq.enqueue(6);
        aq.enqueue(7);

        assertEquals(3, aq.front());
        aq.dequeue();
        assertEquals(5, aq.front());
        aq.dequeue();
        assertEquals(6, aq.front());
    }
}