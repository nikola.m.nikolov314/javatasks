package testsweekone;

import org.junit.jupiter.api.Test;
import tasksweekone.solutions.FindSum;

import static org.junit.jupiter.api.Assertions.*;

class FindSumTest {

    @Test
    void StringWithoutNumbers()  {
        assertEquals(0, FindSum.findSum("Hello there i am Jon Doe"));
        assertEquals(0, FindSum.findSum("asdadasdasddas"));
        assertEquals(0, FindSum.findSum("              "));
        assertEquals(0, FindSum.findSum("/////////////////"));
        assertEquals(0, FindSum.findSum("*&@#$@@DSXSDDSD&"));
    }

    @Test
    void EmptyString() {
        assertEquals(0, FindSum.findSum(""));
    }

    @Test
    void StringWithNumbers() {
        assertEquals(22, FindSum.findSum("I am number 2 and im 20"));
        assertEquals(214, FindSum.findSum("I am number 234 and im -20"));
        assertEquals(0, FindSum.findSum("I am number 2 and im -2"));
        assertEquals(757, FindSum.findSum("I am number 757 and im 20 and i love -20"));
        assertEquals(25, FindSum.findSum("I am number 25 and im 0"));
        assertEquals(220, FindSum.findSum("77 loves 55 but hates 88"));
    }
}