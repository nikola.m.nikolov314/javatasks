package testsweekone;

import org.junit.jupiter.api.Test;

import tasksweekone.solutions.DoubleFact;

import static org.junit.jupiter.api.Assertions.*;

class DoubleFactTest {
    @Test
    void ReturnOneForParameterOne() {
        assertEquals(1, DoubleFact.doubleFact(1));
    }

    @Test
    void ReturnOneFroParameterZero() {
        assertEquals(1, DoubleFact.doubleFact(0));
    }

    @Test
    void ReturnDoubleFactOfN() {
        assertEquals(720, DoubleFact.doubleFact(3));
        assertEquals(2, DoubleFact.doubleFact(2));
    }
}