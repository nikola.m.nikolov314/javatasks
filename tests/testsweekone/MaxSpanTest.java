package testsweekone;

import org.junit.jupiter.api.Test;
import tasksweekone.solutions.MaxSpan;

import static org.junit.jupiter.api.Assertions.*;

class MaxSpanTest {

    @Test
    void ReturnZeroForEmptyArray() {
        int[] numbers = {};
        assertEquals(0, MaxSpan.maxSpan(numbers));
    }

    @Test
    void ReturnZeroForArrayWithoutRepetitiveNumbers() {
        int[] numbers = {1, 2, 3, 4, 5};
        int[] numbers1 = {5, 4, 3, 2, 1};
        int[] numbers2 = {11, 12, 23, 45, 90};
        assertEquals(0, MaxSpan.maxSpan(numbers));
        assertEquals(0, MaxSpan.maxSpan(numbers1));
        assertEquals(0, MaxSpan.maxSpan(numbers2));
    }

    @Test
    void ReturnMaxSpanForArrayWithRepetitiveNumbers() {
        int[] numbers = {1, 2, 3, 1, 10, 22};
        int[] numbers1 = {22, 2, 3, 9, 10, 22};
        int[] numbers2 = {1, 2, 3, 1, 1, 22};
        int[] numbers3 = {1, 2, 3, 1, 10, 2};
        int[] numbers4 = {-1, 2, 3, 1, 10, -1};

        assertEquals(2, MaxSpan.maxSpan(numbers));
        assertEquals(4, MaxSpan.maxSpan(numbers1));
        assertEquals(3, MaxSpan.maxSpan(numbers2));
        assertEquals(3, MaxSpan.maxSpan(numbers3));
        assertEquals(4, MaxSpan.maxSpan(numbers4));
    }
}