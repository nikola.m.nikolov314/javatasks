package testsweekone;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tasksweekone.solutions.MinElement;

import static org.junit.jupiter.api.Assertions.*;

class MinElementTest {

    @Test
    void ReturnZeroForAnEmptyArray() {
        int[] numbers = {};
        Assertions.assertEquals(0, tasksweekone.solutions.MinElement.minElement(numbers));
    }

    @Test
    void ReturnTheMinElementOfANonEmptyArray() {
        int[] numbers = {22, -1, 41, 42};
        int[] numbers1 = {-22, -1, 41, 42, 50};
        int[] numbers2 = {22, -1, 41, -42};
        int[] numbers3 = {22, -1, 41, 42, -90};
        int[] numbers4 = {22, -1, 41, 42, 100};

        assertEquals(-1, MinElement.minElement(numbers));
        assertEquals(-22, MinElement.minElement(numbers1));
        assertEquals(-42, MinElement.minElement(numbers2));
        assertEquals(-90, MinElement.minElement(numbers3));
        assertEquals(-1, MinElement.minElement(numbers4));
    }
}