package testsweekone;

import org.junit.jupiter.api.Test;
import tasksweekone.solutions.IsNumberPalindrome;

import static org.junit.jupiter.api.Assertions.*;

class IsNumberPalindromeTest {

    @Test
    void ReturnTrueForSingleDigitNumbers() {
        assertTrue(IsNumberPalindrome.isNumberPalindrome(1));
        assertTrue(IsNumberPalindrome.isNumberPalindrome(2));
        assertTrue(IsNumberPalindrome.isNumberPalindrome(3));
        assertTrue(IsNumberPalindrome.isNumberPalindrome(4));
        assertTrue(IsNumberPalindrome.isNumberPalindrome(5));
        assertTrue(IsNumberPalindrome.isNumberPalindrome(6));
    }

    @Test
    void ReturnFalseForNonPalindromes() {
        assertFalse(IsNumberPalindrome.isNumberPalindrome(12));
        assertFalse(IsNumberPalindrome.isNumberPalindrome(1234));
        assertFalse(IsNumberPalindrome.isNumberPalindrome(5612));
        assertFalse(IsNumberPalindrome.isNumberPalindrome(1224));
        assertFalse(IsNumberPalindrome.isNumberPalindrome(25));
        assertFalse(IsNumberPalindrome.isNumberPalindrome(251825));
        assertFalse(IsNumberPalindrome.isNumberPalindrome(8921));
    }

    @Test
    void ReturnTrueForPalindromes() {
        assertTrue(IsNumberPalindrome.isNumberPalindrome(999));
        assertTrue(IsNumberPalindrome.isNumberPalindrome(99));
        assertTrue(IsNumberPalindrome.isNumberPalindrome(22));
        assertTrue(IsNumberPalindrome.isNumberPalindrome(7887));
        assertTrue(IsNumberPalindrome.isNumberPalindrome(1991));
        assertTrue(IsNumberPalindrome.isNumberPalindrome(777));
        assertTrue(IsNumberPalindrome.isNumberPalindrome(123474321));
    }
}