package testsweekone;

import org.junit.jupiter.api.Test;
import tasksweekone.solutions.ReverseString;


import static org.junit.jupiter.api.Assertions.*;

class ReverseStringTest {
    @Test
    void ReturnEmptyStringForAnEmptyString() {
        assertEquals("", ReverseString.reverseString(""));
    }

    @Test
    void ReverseRandomEmptyString() {
        assertEquals("olleh", ReverseString.reverseString("hello"));
        assertEquals("How are you?", ReverseString.reverseString("?uoy era woH"));
        assertEquals("word", ReverseString.reverseString("drow"));

    }
}