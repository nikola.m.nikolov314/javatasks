package testsweekone;

import org.junit.jupiter.api.Test;

import tasksweekone.solutions.KthMin;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class KthMinTest {

    int[] numbers = new int[0];

    int[] numbers1 = {1, 2, 3, 4, 5};
    int[] numbers2 = {6, 2, 7, 18, 8, 24};
    int[] numbers3 = {-1, 22, -13, 15, 0};

    @Test
    void ThrowNoSuchElementExceptionForEmptyArray() throws ArrayIndexOutOfBoundsException {
        assertThrows(NoSuchElementException.class, () -> {
            KthMin.kthMin(0, numbers);});
        assertThrows(NoSuchElementException.class, () -> {
            KthMin.kthMin(1, numbers);});
        assertThrows(NoSuchElementException.class, () -> {KthMin.kthMin(2, numbers);});
        assertThrows(NoSuchElementException.class, () -> {KthMin.kthMin(3, numbers);});
        assertThrows(NoSuchElementException.class, () -> {KthMin.kthMin(4, numbers);});
    }

    @Test
    void ThrowArrayOitOfBoundsExceptionForInvalidK() throws  NoSuchElementException {
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {KthMin.kthMin(6, numbers1);});
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {KthMin.kthMin(5, numbers1);});
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {KthMin.kthMin(-1, numbers1);});
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {KthMin.kthMin(-2, numbers1);});
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {KthMin.kthMin(22, numbers1);});
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {KthMin.kthMin(-6, numbers1);});
    }

    @Test
    void GetTheKthMinForSortedArray() throws ArrayIndexOutOfBoundsException, NoSuchElementException {
        assertEquals(1, KthMin.kthMin(0, numbers1));
        assertEquals(2, KthMin.kthMin(1, numbers1));
        assertEquals(3, KthMin.kthMin(2, numbers1));
        assertEquals(4, KthMin.kthMin(3, numbers1));
        assertEquals(5, KthMin.kthMin(4, numbers1));
    }

    @Test
    void GetTheKthMinForNotSortedArray() throws ArrayIndexOutOfBoundsException, NoSuchElementException {
        assertEquals(2, KthMin.kthMin(0, numbers2));
        assertEquals(6, KthMin.kthMin(1, numbers2));
        assertEquals(7, KthMin.kthMin(2, numbers2));
        assertEquals(8, KthMin.kthMin(3, numbers2));
        assertEquals(18, KthMin.kthMin(4, numbers2));
        assertEquals(24, KthMin.kthMin(5, numbers2));
        assertEquals(-13, KthMin.kthMin(0, numbers3));
        assertEquals(-1, KthMin.kthMin(1, numbers3));
        assertEquals(-0, KthMin.kthMin(2, numbers3));
        assertEquals(15, KthMin.kthMin(3, numbers3));
        assertEquals(22, KthMin.kthMin(4, numbers3));
    }

}