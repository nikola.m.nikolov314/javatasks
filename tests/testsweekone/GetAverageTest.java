package testsweekone;

import org.junit.jupiter.api.Test;

import tasksweekone.solutions.GetAverage;

import static org.junit.jupiter.api.Assertions.*;

class GetAverageTest {

    @Test
    void EmptyArray() {
        int[] numbers = new int[2];
        assertEquals(0, GetAverage.getAverage(numbers));
    }

    @Test
    void NotEmptyArray() {
        int[] numbers = {1, 2, 3};
        int[] numbers1 = {1, 1, 1};
        int[] numbers2 = {1, 2, 3, 4};
        int[] numbers3 = {4, 3, 2, 1};
        int[] numbers4 = {150, 550, 750};

        assertEquals(2, GetAverage.getAverage(numbers));
        assertEquals(1, GetAverage.getAverage(numbers1));
        assertEquals(2, GetAverage.getAverage(numbers2));
        assertEquals(2, GetAverage.getAverage(numbers3));
        assertEquals(483, GetAverage.getAverage(numbers4));
    }
}