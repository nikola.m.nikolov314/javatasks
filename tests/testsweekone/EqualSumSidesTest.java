package testsweekone;

import org.junit.jupiter.api.Test;

import tasksweekone.solutions.EqualSumSides;

import static org.junit.jupiter.api.Assertions.*;

class EqualSumSidesTest {
    @Test
    void ReturnFalseForEmptyArray() {
        int[] numbers = {};
        assertFalse(EqualSumSides.equalSumSides(numbers));
    }

    @Test
    void ReturnFalseForArrayThatDoesNotSatisfyCondition() {
        int[] numbers = {22, 19, 80};
        int[] numbers1 = {22, 33, 19, 80, 50};
        int[] numbers2 = {22, 0, 80};
        int[] numbers3 = {35, 19, 106, 802};
        assertFalse(EqualSumSides.equalSumSides(numbers));
        assertFalse(EqualSumSides.equalSumSides(numbers1));
        assertFalse(EqualSumSides.equalSumSides(numbers2));
        assertFalse(EqualSumSides.equalSumSides(numbers3));
    }

    @Test
    void ReturnTrueForArrayThatSatisfiesCondition() {
        int[] numbers = {1, 2, 1};
        int[] numbers1 = {22, 2, 22};
        int[] numbers2 = {1, 0, 0};
        int[] numbers3 = {1, 0, 1};
        int[] numbers4 = {0, 0, 1};
        int[] numbers5 = {15, -15, 23, 14, -14};
        int[] numbers6 = {15, -15, -14, 14, -14};
        assertTrue(EqualSumSides.equalSumSides(numbers));
        assertTrue(EqualSumSides.equalSumSides(numbers1));
        assertTrue(EqualSumSides.equalSumSides(numbers2));
        assertTrue(EqualSumSides.equalSumSides(numbers3));
        assertTrue(EqualSumSides.equalSumSides(numbers4));
        assertTrue(EqualSumSides.equalSumSides(numbers5));
        assertTrue(EqualSumSides.equalSumSides(numbers6));
    }
}