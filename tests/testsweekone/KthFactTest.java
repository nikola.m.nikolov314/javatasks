package testsweekone;

import org.junit.jupiter.api.Test;

import tasksweekone.solutions.KthFact;

import static org.junit.jupiter.api.Assertions.*;

class KthFactTest {

    @Test
    void ReturnOneForParameterOne() {
        assertEquals(1, KthFact.kthFact(1, 1));
        assertEquals(1, KthFact.kthFact(1, 2));
        assertEquals(1, KthFact.kthFact(1, 3));
        assertEquals(1, KthFact.kthFact(1, 4));
    }

    @Test
    void ReturnOneFroParameterZero() {
        assertEquals(1, KthFact.kthFact(0, 1));
        assertEquals(1, KthFact.kthFact(0, 2));
        assertEquals(1, KthFact.kthFact(0, 3));
        assertEquals(1, KthFact.kthFact(0, 4));
    }

    @Test
    void ReturnKthFactOfN() {
        assertEquals(720, KthFact.kthFact(3, 2));
        assertEquals(2, KthFact.kthFact(2, 2));
    }
}