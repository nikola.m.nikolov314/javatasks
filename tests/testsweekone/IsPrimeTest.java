package testsweekone;

import org.junit.jupiter.api.Test;

import tasksweekone.solutions.IsPrime;

import static org.junit.jupiter.api.Assertions.*;

class IsPrimeTest {

    @Test
    void testPrime() {
        assertTrue(IsPrime.isPrime(3));
        assertTrue(IsPrime.isPrime(11));
        assertTrue(IsPrime.isPrime(13));
        assertTrue(IsPrime.isPrime(17));
        assertTrue(IsPrime.isPrime(23));
    }

    @Test
    void testNotPrime() {
        assertFalse(IsPrime.isPrime(4));
        assertFalse(IsPrime.isPrime(6));
        assertFalse(IsPrime.isPrime(9));
        assertFalse(IsPrime.isPrime(21));
    }

    @Test
    void testZero() {
        assertFalse(IsPrime.isPrime(0));
    }

    @Test
    void testNegative() {
        assertFalse(IsPrime.isPrime(-2));
        assertFalse(IsPrime.isPrime(-3));
        assertFalse(IsPrime.isPrime(-4));
        assertFalse(IsPrime.isPrime(-5));
    }
}