package testsweekone;

import org.junit.jupiter.api.Test;
import tasksweekone.solutions.MaximalScalarSum;

import static org.junit.jupiter.api.Assertions.*;

class MaximalScalarSumTest {

    @Test
    void ReturnZeroForInvalidVectors() {
        int[] vector1 = {1, 2, 3};
        int[] vector2 = {1, 2,};
        int[] vector3 = {1};
        int[] vector4 = {1, 2,};
        int[] vector5 = {1, 2, 3, 4, 5};
        assertEquals(0, MaximalScalarSum.maximalScalarSum(vector1, vector2));
        assertEquals(0, MaximalScalarSum.maximalScalarSum(vector3, vector4));
        assertEquals(0, MaximalScalarSum.maximalScalarSum(vector1, vector5));
    }

    @Test
    void ReturnTheMaxScalarSumForTwoSortedVectors() {
        int[] vector1 = {1, 2, 3};
        int[] vector2 = {5, 6, 7};
        int[] vector3 = {3, 4, 5};
        int[] vector5 = {10, 20, 30};
        assertEquals(38, MaximalScalarSum.maximalScalarSum(vector1, vector2));
        assertEquals(74, MaximalScalarSum.maximalScalarSum(vector2, vector3));
        assertEquals(140, MaximalScalarSum.maximalScalarSum(vector1, vector5));
        assertEquals(260, MaximalScalarSum.maximalScalarSum(vector5, vector3));
    }

    @Test
    void ReturnTheMaxScalarSumForTwoNonSortedVectors() {
        int[] vector1 = {3, 2, 1};
        int[] vector2 = {5, 7, 6};
        int[] vector3 = {5, 4, 3};
        int[] vector5 = {20, 10, 30};
        assertEquals(38, MaximalScalarSum.maximalScalarSum(vector1, vector2));
        assertEquals(74, MaximalScalarSum.maximalScalarSum(vector2, vector3));
        assertEquals(140, MaximalScalarSum.maximalScalarSum(vector1, vector5));
        assertEquals(260, MaximalScalarSum.maximalScalarSum(vector5, vector3));
    }
}