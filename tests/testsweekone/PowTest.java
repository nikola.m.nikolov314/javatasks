package testsweekone;

import org.junit.jupiter.api.Test;
import tasksweekone.exceptions.InvalidPowerException;
import tasksweekone.solutions.Pow;

import static org.junit.jupiter.api.Assertions.*;

class PowTest {
    @Test
    void testZero() throws InvalidPowerException {
        assertEquals(1, Pow.pow(3, 0));
        assertEquals(1, Pow.pow(2, 0));
        assertEquals(1, Pow.pow(1, 0));
        assertEquals(1, Pow.pow(4, 0));
        assertEquals(1, Pow.pow(5, 0));
    }

    @Test
    void testOne() throws InvalidPowerException{
        assertEquals(1, Pow.pow(1, 1));
        assertEquals(2, Pow.pow(2, 1));
        assertEquals(44, Pow.pow(44, 1));
        assertEquals(222, Pow.pow(222, 1));
        assertEquals(1768, Pow.pow(1768, 1));
    }

    @Test
    void testGreaterThanOne() throws InvalidPowerException {
        assertEquals(8, Pow.pow(2, 3));
        assertEquals(4, Pow.pow(2, 2));
        assertEquals(81, Pow.pow(3, 4));
        assertEquals(225, Pow.pow(15, 2));
        assertEquals(1000, Pow.pow(10, 3));
        assertEquals(48828125, Pow.pow(5, 11));

        assertNotEquals(14, Pow.pow(2, 6));
        assertNotEquals(15, Pow.pow(5, 3));
        assertNotEquals(24, Pow.pow(2, 6));
        assertNotEquals(18, Pow.pow(2, 6));
    }

    @Test
    void testNegative()  {
        assertThrows(InvalidPowerException.class,() ->{Pow.pow(2, -3);} );
    }
}