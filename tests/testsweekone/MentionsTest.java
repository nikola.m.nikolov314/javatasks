package testsweekone;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tasksweekone.solutions.Mentions;

import static org.junit.jupiter.api.Assertions.*;

class MentionsTest {

    @Test
    void ReturnZeroForLookingIntoEmptyString() {
        assertEquals(0, Mentions.mentions("", "cat"));
        assertEquals(0, Mentions.mentions("", "asddasd"));
        assertEquals(0, Mentions.mentions("", "dog"));

    }

    @Test
    void ReturnOneForCountingEmptyStringIntoEmptyString() {
        assertEquals(1, tasksweekone.solutions.Mentions.mentions("", ""));
    }

    @Test
    void ReturnZeroForAWordWhichIsNotInTheString() {
        assertEquals(0, tasksweekone.solutions.Mentions.mentions("hello world", "worldd"));
        assertEquals(0, tasksweekone.solutions.Mentions.mentions("just a random string", "word"));
        assertEquals(0, tasksweekone.solutions.Mentions.mentions("another random string", "stringbuilder"));
        assertEquals(0, tasksweekone.solutions.Mentions.mentions("hello brave new world", "helo"));
    }
}