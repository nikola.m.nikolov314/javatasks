package testsweekone;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HasAnagramOfTest {
    @Test
    void ReturnFalseForStringsThatDoNotSatisfyTheCondition() {
        Assertions.assertFalse(tasksweekone.solutions.HasAnagramOf.hasAnagramOf("hello there", "cat"));
        assertFalse(tasksweekone.solutions.HasAnagramOf.hasAnagramOf("How are you", "dog"));
        assertFalse(tasksweekone.solutions.HasAnagramOf.hasAnagramOf("Random string", "fly"));
    }

    @Test
    void ReturnTrueForStringsThatSatisfyTheCondition() {
        assertTrue(tasksweekone.solutions.HasAnagramOf.hasAnagramOf("you have a nice cat", "tac"));
        assertTrue(tasksweekone.solutions.HasAnagramOf.hasAnagramOf("you have a fast car", "arc"));
        assertTrue(tasksweekone.solutions.HasAnagramOf.hasAnagramOf("another random string", "odnarm"));
    }
}

