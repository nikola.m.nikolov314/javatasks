package testsweekone;

import org.junit.jupiter.api.Test;

import tasksweekone.solutions.GetLargestPalindrome;

import static org.junit.jupiter.api.Assertions.*;

class GetLargestPalindromeTest {

    @Test
    void ReturnNForOneDigitNumbers() {
        assertEquals(1, GetLargestPalindrome.getLargestPalindrome(1));
        assertEquals(2, GetLargestPalindrome.getLargestPalindrome(2));
        assertEquals(3, GetLargestPalindrome.getLargestPalindrome(3));
        assertEquals(4, GetLargestPalindrome.getLargestPalindrome(4));
        assertEquals(5, GetLargestPalindrome.getLargestPalindrome(5));
        assertEquals(6, GetLargestPalindrome.getLargestPalindrome(6));
        assertEquals(7, GetLargestPalindrome.getLargestPalindrome(7));
    }

    @Test
    void ReturnLargestPalindromeForNDigitNumber() {
        assertEquals(999, GetLargestPalindrome.getLargestPalindrome(1000));
        assertEquals(99, GetLargestPalindrome.getLargestPalindrome(100));
        assertEquals(55, GetLargestPalindrome.getLargestPalindrome(60));
        assertEquals(66, GetLargestPalindrome.getLargestPalindrome(70));
        assertEquals(6996, GetLargestPalindrome.getLargestPalindrome(7000));
        assertEquals(7997, GetLargestPalindrome.getLargestPalindrome(8000));
        assertEquals(9999, GetLargestPalindrome.getLargestPalindrome(10000));
        assertEquals(24942, GetLargestPalindrome.getLargestPalindrome(25000));
    }


}