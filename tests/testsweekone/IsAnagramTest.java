package testsweekone;
import org.junit.jupiter.api.Test;
import tasksweekone.solutions.IsAnagram;

import static org.junit.jupiter.api.Assertions.*;

class IsAnagramTest {

    @Test
    void ReturnTrueForSingleLetters() {
        assertTrue(IsAnagram.isAnagram("a", "a"));
        assertTrue(IsAnagram.isAnagram("b", "b"));
        assertTrue(IsAnagram.isAnagram("c", "c"));
        assertTrue(IsAnagram.isAnagram("d", "d"));
    }

    @Test
    void ReturnFalseForSingleLetters() {
        assertFalse(IsAnagram.isAnagram("a", "b"));
        assertFalse(IsAnagram.isAnagram("b", "c"));
        assertFalse(IsAnagram.isAnagram("c", "a"));
        assertFalse(IsAnagram.isAnagram("d", "g"));
    }

    @Test
    void ReturnTrueForAnagrams() {
        assertTrue(IsAnagram.isAnagram("JimMorrison", "MrMojoRisin"));
        assertTrue(IsAnagram.isAnagram("DamonAlbarn", "DanAbnormal"));
        assertTrue(IsAnagram.isAnagram("GeorgeBush", "HebugsGore"));
        assertTrue(IsAnagram.isAnagram("ClintEastwood", "OldWestaction"));
    }

    @Test
    void ReturnFalseForNonAnagrams() {
        assertFalse(IsAnagram.isAnagram("JimMorrison", "HebugsGore"));
        assertFalse(IsAnagram.isAnagram("ClintEastwood", "HebugsGore"));
        assertFalse(IsAnagram.isAnagram("ClintEastwood", "HebugsGore"));
        assertFalse(IsAnagram.isAnagram("GeorgeBush", "DanAbnormal"));
    }
}