package testsweekone;

import org.junit.jupiter.api.Test;
import tasksweekone.solutions.IsOdd;

import static org.junit.jupiter.api.Assertions.*;

class IsOddTest {
    @Test
    void testOdd() {
        assertTrue(IsOdd.isOdd(3));
        assertTrue(IsOdd.isOdd(5));
        assertTrue(IsOdd.isOdd(7));
        assertTrue(IsOdd.isOdd(9));
        assertTrue(IsOdd.isOdd(11));
    }

    @Test
    void testEven() {
        assertFalse(IsOdd.isOdd(2));
        assertFalse(IsOdd.isOdd(22));
        assertFalse(IsOdd.isOdd(12));
        assertFalse(IsOdd.isOdd(14));
    }

    @Test
    public void testZero() {
        assertFalse(IsOdd.isOdd(0));
    }

    @Test
    public void testNegativeOdd() {
        assertTrue(IsOdd.isOdd(-3));
        assertTrue(IsOdd.isOdd(-5));
        assertTrue(IsOdd.isOdd(-9));
        assertTrue(IsOdd.isOdd(-11));
    }

    @Test
    public void testNegativeEven() {
        assertFalse(IsOdd.isOdd(-2));
        assertFalse(IsOdd.isOdd(-4));
        assertFalse(IsOdd.isOdd(-6));
        assertFalse(IsOdd.isOdd(-8));
        assertFalse(IsOdd.isOdd(-10));
        assertFalse(IsOdd.isOdd(-22));
    }
}