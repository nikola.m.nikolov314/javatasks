package testsweekone;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tasksweekone.solutions.ReverseEachWord;

import static org.junit.jupiter.api.Assertions.*;

class ReverseEachWordTest {
    @Test
    void ReturnEmptyStringForAnEmptyString() {
        assertEquals("", ReverseEachWord.reverseEachWord(""));
    }

    @Test
    void ReverseEachWordInARandomString() {
        assertEquals("olleh ereht", ReverseEachWord.reverseEachWord("hello there"));
        assertEquals("modnar gnirts", ReverseEachWord.reverseEachWord("random string"));
        assertEquals("rehtona modnar gnirts", ReverseEachWord.reverseEachWord("another random string"));
    }
}