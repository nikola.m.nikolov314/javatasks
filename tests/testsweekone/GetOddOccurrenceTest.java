package testsweekone;

import org.junit.jupiter.api.Test;

import tasksweekone.solutions.GetOddOccurrence;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class GetOddOccurrenceTest {
    @Test
    void ThrowNoSuchElement() {
        int[] numbers = {1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 7, 7};
        int[] numbers1 = {22, 22, 33, 33};
        int[] numbers2 = {18, 18};
        int[] numbers3 = {};
        assertThrows(NoSuchElementException.class, () ->{ GetOddOccurrence.getOddOccurrence(numbers);});
        assertThrows(NoSuchElementException.class, () ->{GetOddOccurrence.getOddOccurrence(numbers2);});
        assertThrows(NoSuchElementException.class, () ->{GetOddOccurrence.getOddOccurrence(numbers1);});
        assertThrows(NoSuchElementException.class, () ->{GetOddOccurrence.getOddOccurrence(numbers3);});
    }

    @Test
    void ReturnTheFirstNumberThatOccursOddTimesInSortedArray() {
        int[] numbers = {1, 2, 3, 3, 4};
        int[] numbers1 = {22, 0, 33, 33};
        int[] numbers2 = {18, 19};
        assertEquals(1,GetOddOccurrence.getOddOccurrence(numbers));
        assertEquals(0,GetOddOccurrence.getOddOccurrence(numbers1));
        assertEquals(18,GetOddOccurrence.getOddOccurrence(numbers2));

    }
}