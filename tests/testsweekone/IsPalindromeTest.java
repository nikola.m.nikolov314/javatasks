package testsweekone;

import org.junit.jupiter.api.Test;
import tasksweekone.solutions.IsPalindrome;

import static org.junit.jupiter.api.Assertions.*;

class IsPalindromeTest {

    @Test
    void SingleCharIsPalindrome() {
        assertTrue(IsPalindrome.isPalindrome("a"));
        assertTrue(IsPalindrome.isPalindrome("b"));
        assertTrue(IsPalindrome.isPalindrome("c"));
        assertTrue(IsPalindrome.isPalindrome("f"));
        assertTrue(IsPalindrome.isPalindrome("e"));
        assertTrue(IsPalindrome.isPalindrome("g"));
    }

    @Test
    void EmptyStringIsPalindrome() {
        assertTrue(IsPalindrome.isPalindrome(""));
    }

    @Test
    void StringIsNotPalindrome() {
        assertFalse(IsPalindrome.isPalindrome("asd"));
        assertFalse(IsPalindrome.isPalindrome("assd"));
        assertFalse(IsPalindrome.isPalindrome("as"));
    }

    @Test
    void StringIsPalindrome() {
        assertTrue(IsPalindrome.isPalindrome("bob"));
        assertTrue(IsPalindrome.isPalindrome("abba"));
        assertTrue(IsPalindrome.isPalindrome("abba"));
        assertTrue(IsPalindrome.isPalindrome("kayak"));
        assertTrue(IsPalindrome.isPalindrome("level"));
    }
}