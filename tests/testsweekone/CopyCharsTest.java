package testsweekone;

import org.junit.jupiter.api.Test;

import tasksweekone.solutions.CopyChars;

import static org.junit.jupiter.api.Assertions.*;

class CopyCharsTest {
    @Test
    void DoNotCopyCharsWhenPassedZero() {
        assertEquals("", CopyChars.copyChars("asd", 0));
        assertEquals("", CopyChars.copyChars("aaaa", 0));
    }

    @Test
    void ReturnEmptyStringWhenPassedEmptyString() {
        assertEquals("", CopyChars.copyChars("", 0));
        assertEquals("", CopyChars.copyChars("", 1));
        assertEquals("", CopyChars.copyChars("", 2));
    }

    @Test
    void ReturnPassedStringCopiedKTimes() {
        assertEquals("asdasd", CopyChars.copyChars("asd", 2));
        assertEquals("aaaa", CopyChars.copyChars("a", 4));
        assertEquals("hello", CopyChars.copyChars("hello", 1));
        assertEquals("hehehehehe", CopyChars.copyChars("he", 5));
    }
}